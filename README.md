# README #

## clone代码到本地

## 配置环境
* 安装NodeJS(版本0.10.x)
* 安装MongoDB
* 安装redis
* 安装sharp依赖: http://sharp.dimens.io/en/stable/install/

## 安装工具和依赖(失败请翻墙)
* npm install gulp -g
* npm install nodemon -g (这个是监控文件变化自动重启服务器的工具)
* cd app/
* npm install (node-gyp编译卡住可能是你Node源码缺失)
* cd prerender-master
* npm install  


## 启动主程序
* cd app/
* npm start 或者 nodemon ./bin/www

* http://localhost:8888
* http://localhost:8888/duwei  (后台URL)

## 启动预渲染(只对搜索引擎爬虫有效，会自动渲染angularjs页面生成静态HTML给爬虫,本地环境可以不启动)
* cd prerender-master/
* node server.js  (6000端口)

## 开发 (less文件夹下暂时是CSS)
* `gulp` --合并编译前端所有less和JS并压缩  -> public/frontend/dist/
* `gulp frontend` --立即编译frontend所有less和JS并压缩,监控frontend/中的LESS和JS变动并立即执行编译合并压缩(frontend下需新建less文件夹)
* `gulp backend` --立即编译backend所有less和JS并压缩,监控backend/中的LESS和JS变动并立即执行编译合并压缩(backend下需新建less文件夹)

## 目录结构
* prerender-master -- 页面预渲染器（搜索引擎爬虫访问会通过它返回渲染完成的页面）
* app -- 主目录
* /app/node_modules  --各种包
* /app/bin -- 执行程序
* /app/controllers --控制器
* /app/middlewares --中间件
* /app/routes --路由
* /app/models --数据模型
* /app/public --静态文件
* /app/proxy  --重用方法代理(本来应该是数据代理层...后来感觉取数据不太复杂...)
* app.js  
* config.js --配置文件





