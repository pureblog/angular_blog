(function () {
  
	'use strict';
  
	var express = require('express'),
		path = require('path'),
		favicon = require('serve-favicon'),
		logger = require('morgan'),
		bodyParser = require('body-parser'),
		config = require('./config'),
		frontend = require('./routes/frontend'),
		backend = require('./routes/backend'),
		app = express();

	app.use(require('prerender-node').set('prerenderServiceUrl', 'http://localhost:6000/'));

	// uncomment after placing your favicon in /public
	//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
	app.use(logger('dev'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(express.static(path.join(__dirname, 'public')));
	app.use('/upload', express.static(path.join(__dirname, 'upload')));

	app.use(config.backendUrl, backend);
	app.use('/', frontend); //一定要放在后面   

	// catch 404 and forward to error handler
	app.use(function (req, res, next) {
		var err = new Error('Not Found');
		err.status = 404;
		next(err);
	});

	// error handlers

	// development error handler
	if (app.get('env') === 'development') {
		app.use(function (err, req, res, next) {
			if (err.kind === 'ObjectId') {
				return res.json({
					success: 0,
					message: '请提供正确的ObjectId！'
				});
			} else {
				res.status(err.status || 500);
				console.error(err.status, err);
			}
		});
	}

	// production error handler
	app.use(function (err, req, res, next) {
		if (err.kind !== 'ObjectId') {
			res.status(err.status || 500);
			console.error(err.status, err);
		}
	});


	module.exports = app;
	
}());
