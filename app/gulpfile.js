(function () {
	
	'use strict';

	var gulp = require('gulp'),
		uglify = require('gulp-uglify'),
		minifyCss = require('gulp-minify-css'),
		concat = require('gulp-concat'),
		notify = require('gulp-notify'),
		rename = require('gulp-rename'),
		less = require('gulp-less'),
		plumber = require('gulp-plumber');

	var LessPluginCleanCSS = require('less-plugin-clean-css'),
	    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	    cleancss = new LessPluginCleanCSS({ advanced: true }),
	    autoprefix= new LessPluginAutoPrefix({ browsers: ["last 10 versions"] });

	gulp.task('frontendJS', function () {
		return gulp.src('public/frontend/js/**/*.js')
			.pipe(concat('frontend.js'))
			.pipe(gulp.dest('public/frontend/dist/assets/js'))
			.pipe(rename({suffix: '.min'}))
			.pipe(uglify())
			.pipe(gulp.dest('public/frontend/dist/assets/js'));
	});

	gulp.task('backendJS', function () {
		return gulp.src('public/backend/js/**/*.js')
			.pipe(concat('backend.js'))
			.pipe(gulp.dest('public/backend/dist/assets/js'))
			.pipe(rename({suffix: '.min'}))
			.pipe(uglify())
			.pipe(gulp.dest('public/backend/dist/assets/js'));
	});

	gulp.task('frontendLess', function () {
		return gulp.src('public/frontend/less/**/*.less')
			.pipe(concat('frontend.less', {newLine: ' '}))
			.pipe(plumber())
			.pipe(less({
	  		plugins: [autoprefix]
	  	}))
	  	.on('error', function(e){console.log(e);} )
	  	.pipe(gulp.dest('public/frontend/dist/assets/css'))
	  	.pipe(rename({suffix: '.min'}))
			.pipe(minifyCss())
    		.pipe(gulp.dest('public/frontend/dist/assets/css'));
	});
	
	gulp.task('backendLess', function () {
		return gulp.src('public/backend/less/**/*.less')
			.pipe(concat('backend.less', {newLine: ' '}))
			.pipe(plumber())
			.pipe(less({
	  		plugins: [autoprefix]
	  	}))
	  	.on('error', function(e){console.log(e);} )
	  	.pipe(gulp.dest('public/backend/dist/assets/css'))
	  	.pipe(rename({suffix: '.min'}))
		.pipe(minifyCss())
    		.pipe(gulp.dest('public/backend/dist/assets/css'));
	});

	gulp.task('frontend', ['frontendLess', 'frontendJS'], function () {
	  gulp.watch('public/frontend/less/**/*.less', ['frontendLess']);
	  gulp.watch('public/frontend/js/**/*.js', ['frontendJS']);
	});

	gulp.task('backend', ['backendLess', 'backendJS'], function () {
	  gulp.watch('public/backend/less/**/*.less', ['backendLess']);
	  gulp.watch('public/backend/js/**/*.js', ['backendJS']);
	});

	gulp.task('default', function () {
		gulp.start('frontendJS', 'backendJS', 'frontendLess', 'backendLess');
	});

}());