(function () {

	'use strict';
	
	var app = angular.module('app.directive', []);
	app.directive('compareTo', function () {
		return {
			require: 'ngModel',
			scope: {
				otherModelValue: '=compareTo'
			},
			link: function (scope, element, attributes, ngModel) {
				ngModel.$validators.compareTo = function (modelValue) {
					return modelValue === scope.otherModelValue;
				};
				scope.$watch('otherModelValue', function () {
					ngModel.$validate();
				});
			}
		};
	});
	app.directive('editor', ['$localStorage', '$sessionStorage', function ($localStorage, $sessionStorage) {
		return {
			restrict: 'E',
			replace: true,
			template: '<div id="editormd"></div>',
			link: function ($scope) {
				var token = $localStorage.token || $sessionStorage.token || '';
				var h = $(window).height() - $('#editor-header').outerHeight() - 2;
				var windowX = $(window).width();
				var showLineNum = windowX > 960 ? true : false;
				var editor = $scope.editor = editormd('editormd', {
					path: '/lib/editormd/lib/',
					height: h,
					markdown: $scope.post.md,
					watch: true,
					lineNumbers: showLineNum,
					htmlDecode: 'style,script,iframe',
					emoji: true,
					taskList: true,
					tocm: false, // Using [TOCM]
					tex: false, // 开启科学公式TeX语言支持，默认关闭
					flowChart: true, // 开启流程图支持，默认关闭
					sequenceDiagram: true, // 开启时序/序列图支持，默认关闭,
					imageUpload: true,
					imageFormats: ['jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'png', 'PNG', 'bmp', 'BMP', 'webp', 'WEBP'],
					imageUploadURL: '/duwei/x_upload/post/' + token,
					/*
					 上传的后台只需要返回一个JSON数据，结构如下：
					 {
					 success : 0 | 1,           // 0 表示上传失败，1 表示上传成功
					 message : "提示的信息，上传成功或上传失败及错误信息等。",
					 url     : "图片地址"        // 上传成功时才返回
					 }
					 */
					toolbarIcons: function () {
						if (windowX <= 600) {
							return ['undo', 'redo', '|', 'image', 'preview'];
						} else if (windowX > 600 && windowX <= 960) {
							return editormd.toolbarModes['simple'];
						} else {
							return editormd.toolbarModes['full'];
						}
					},
					onload: function () {
						if (windowX <= 600) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else if (windowX > 600 && windowX <= 960) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else {
							this.preview.show();
							this.codeMirror.css('width', this.editor.width() / 2);
						}
						var keyMap = {
							'Ctrl-S': $scope.savePost
						}
						if ($scope.post.author && Number($scope.global_user.power) === 0 && $scope.post.author._id.toString() !== $scope.global_user._id.toString()) {
							console.log('power limit!');
						} else {
							this.addKeyMap(keyMap);
						}
					},
					onresize: function () {
						var windowX = $(window).width();
						if (windowX <= 600) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else if (windowX > 600 && windowX <= 960) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else {
							this.preview.show();
							this.codeMirror.css('width', this.editor.width() / 2);
						}
					},
					onfullscreen: function () {
						$('#site-nav').hide();
					},
					onfullscreenExit: function () {
						$('#site-nav').show();
					},
					onpreviewing: function () {


					},
					onpreviewed: function () {

					}
				});
			}
		}
	}]);

}());