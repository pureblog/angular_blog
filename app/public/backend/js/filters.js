(function () {

	'use strict';
	
	var app = angular.module('app.filter', []);

	app.filter('publish', function () {
		return function (value) {
			if (value === true) {
				return '已发布';
			} else {
				return '未发布';
			}
		}
	});

	app.filter('power', function () {
		return function (value) {
			switch (value) {
			case 0:
				return '作者';
			case 1:
				return '管理员';
			case 2:
				return 'master';
			}
		}
	});

}());