(function () {

	'use strict';
	
	var app = angular.module('Backend', [
		'ui.router',
		'oc.lazyLoad',
		'http-auth-interceptor',
		'ngStorage',
		'ngFileUpload',
		'ngMaterial',
		'ngMessages',
		'app.controller',
		'app.service',
		'app.directive',
		'app.filter'
	]);
	app.config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
		$stateProvider
			.state('main', {
				abstract: true,
				url: '/duwei',
				templateUrl: '/backend/pages/layout.html',
			})
			.state('error', {
				url: '/duwei/error',
				templateUrl: '/backend/pages/error.html',
				controller: 'ErrorCtrl'
			})
			.state('signin', {
				url: '/duwei/signin',
				templateUrl: '/backend/pages/signin.html'
			})
			.state('signup', {
				url: '/duwei/signup',
				templateUrl: '/backend/pages/signup.html',
				controller: 'SignupCtrl',
				resolve: {
					checkKey: ['SignService', '$location', function (SignService, $location) {
						return SignService.checkSignup($location.search());
					}]
				}
			})
			.state('resetpass', {
				url: '/duwei/resetpass',
				templateUrl: '/backend/pages/reset_pass.html',
				controller: 'ResetPassCtrl',
				resolve: {
					checkKey: ['SignService', '$location', function (SignService, $location) {
						return SignService.checkReset($location.search());
					}]
				}
			})
			.state('main.index', {
				url: '/index',
				templateUrl: '/backend/pages/index.html',
				controller: 'IndexCtrl',
				resolve: {
					'user': ['UserService', '$rootScope', '$localStorage', '$sessionStorage', function (UserService, $rootScope, $localStorage, $sessionStorage) {
						var id;
						if ($localStorage.user) {
							id = $localStorage.user._id;
							return UserService.getOneUser(id);
						} else if ($sessionStorage.user) {
							id = $sessionStorage.user._id;
							return UserService.getOneUser(id);
						} else {
							return $rootScope.$broadcast('event:auth-loginRequired');
						}
						return UserService.getOneUser(id);
					}],
					'settings': ['SettingsService', function (SettingsService) {
						return SettingsService.getSettings();
					}]
				}
			})
			.state('main.editor', {
				url: '/editor/:id',
				templateUrl: '/backend/pages/editor.html',
				controller: 'EditorCtrl',
				resolve: {
					loadcss: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('/lib/editormd/css/editormd.min.css');
					}],
					loadJS: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							serie: true,
							files: [
								'//cdn.bootcss.com/jquery/2.1.4/jquery.min.js',
								'/lib/editormd/editormd.min.js'
							]
						});
					}],
					post: ['PostService', '$stateParams', function (PostService, $stateParams) {
						var id = $stateParams.id || '';
						return PostService.getOnePost(id);
					}],
					'classify': ['PostService', function (PostService) {
						return PostService.getClassify();
					}]
				}
			})
			.state('main.posts', {
				url: '/posts',
				templateUrl: '/backend/pages/posts.html',
				controller: 'PostsCtrl',
				resolve: {
					'classify': ['PostService', function (PostService) {
						return PostService.getClassify();
					}],
					'author': ['UserService', function (UserService) {
						return UserService.getUsers({
							'active': true
						});
					}],
					'posts': ['PostService', function (PostService) {
						return PostService.getPosts();
					}]
				}
			})
			.state('main.users', {
				url: '/users',
				templateUrl: '/backend/pages/users.html',
				controller: 'UsersCtrl',
				resolve: {
					'users': ['UserService', function (UserService) {
						return UserService.getUsers();
					}]
				}
			})
			.state('main.user', {
				url: '/user/:id',
				templateUrl: '/backend/pages/edit_user.html',
				controller: 'UserCtrl',
				resolve: {
					'user': ['UserService', '$stateParams', function (UserService, $stateParams) {
						var id = $stateParams.id;
						return UserService.getOneUser(id);
					}]
				}
			})
			.state('main.settings', {
				url: '/settings',
				templateUrl: '/backend/pages/settings.html',
				controller: 'SettingsCtrl',
				resolve: {
					'settings': ['SettingsService', function (SettingsService) {
						return SettingsService.getSettings();
					}]
				}
			});
		$urlRouterProvider
			.when('/duwei', '/duwei/index')
			.when('/duwei/', '/duwei/index')
			.when('/duwei/editor', '/duwei/editor/')
			.otherwise('/duwei/error');
	}]);
	app.config(['$mdIconProvider',function ($mdIconProvider) {
		$mdIconProvider
			.icon('home', '/fonts/basic_home.svg')
			.icon('add', '/fonts/arrows_plus.svg')
			.icon('posts', '/fonts/basic_folder_multiple.svg')
			.icon('users', '/fonts/basic_postcard_multiple.svg')
			.icon('adduser', '/fonts/basic_elaboration_browser_plus.svg')
			.icon('settings', '/fonts/basic_gear.svg')
			.icon('lock', '/fonts/basic_lock.svg')
			.icon('enter', '/fonts/basic_paperplane.svg')
			.icon('close', '/fonts/arrows_remove.svg')
			.icon('mail', '/fonts/basic_mail.svg')
			.icon('qq', '/fonts/qq.svg')
			.icon('phone', '/fonts/basic_smartphone.svg')
			.icon('yes', '/fonts/arrows_check.svg')
			.icon('up', '/fonts/arrows_up.svg')
			.icon('right', '/fonts/arrows_right.svg')
			.icon('upload', '/fonts/basic_elaboration_cloud_upload.svg')
			.icon('delete', '/fonts/basic_trashcan.svg')
			.icon('send', '/fonts/basic_elaboration_cloud_check.svg')
			.icon('save', '/fonts/basic_elaboration_calendar_check.svg')
			.icon('undo', '/fonts/arrows_anticlockwise.svg')
			.icon('menu', '/fonts/arrows_hamburger1.svg')
			.icon('info', '/fonts/basic_info.svg')
			.icon('link', '/fonts/basic_link.svg')
			.icon('img', '/fonts/basic_picture.svg')
			.icon('tag', '/fonts/ecommerce_sale.svg')
			.icon('edit', '/fonts/software_pencil.svg')
			.icon('date', '/fonts/basic_calendar.svg')
			.icon('refresh', '/fonts/arrows_rotate.svg')
			.icon('male', '/fonts/basic_male.svg')
			.icon('female', '/fonts/basic_female.svg');
	}]);
	app.run(['$http', '$templateCache', function ($http, $templateCache) {
		var urls = [
			'/fonts/basic_home.svg',
			'/fonts/arrows_plus.svg',
			'/fonts/basic_folder_multiple.svg',
			'/fonts/basic_postcard_multiple.svg',
			'/fonts/basic_elaboration_browser_plus.svg',
			'/fonts/basic_gear.svg',
			'/fonts/basic_lock.svg',
			'/fonts/basic_paperplane.svg',
			'/fonts/arrows_remove.svg',
			'/fonts/basic_mail.svg',
			'/fonts/qq.svg',
			'/fonts/basic_smartphone.svg',
			'/fonts/arrows_check.svg',
			'/fonts/arrows_up.svg',
			'/fonts/basic_elaboration_cloud_upload.svg',
			'/fonts/basic_trashcan.svg',
			'/fonts/basic_elaboration_cloud_check.svg',
			'/fonts/basic_elaboration_calendar_check.svg',
			'/fonts/arrows_anticlockwise.svg',
			'/fonts/arrows_hamburger1.svg',
			'/fonts/basic_info.svg',
			'/fonts/basic_link.svg',
			'/fonts/basic_picture.svg',
			'/fonts/ecommerce_sale.svg',
			'/fonts/software_pencil.svg',
			'/fonts/basic_calendar.svg',
			'/fonts/arrows_rotate.svg',
			'/fonts/basic_male.svg',
			'/fonts/basic_female.svg',
		];
		angular.forEach(urls, function (url) {
			$http.get(url, {
				cache: $templateCache
			});
		});
	}]);
	app.run([
		'$rootScope',
		'$state',
		'$http',
		'SignService',
		'ToastService',
		'$localStorage',
		'$sessionStorage',
		function ($rootScope, $state, $http, SignService, ToastService, $localStorage, $sessionStorage) {
			$rootScope.$on('$stateChangeStart', function (ev, toState, toParams, fromState, fromParams) {
				$rootScope.global_user = $localStorage.user || $sessionStorage.user || '';
				var hideSiteheader = ['main.editor']; //需要隐藏 header 的 路由状态
				var hideSiteFooter = ['main.editor', 'main.settings']; //需要隐藏 footer 的 路由状态
				var token = $localStorage.token || $sessionStorage.token || $http.defaults.headers.common.accessToken;
				$rootScope.global_showSiteheader = hideSiteheader.indexOf(toState.name) < 0 ? true : false;
				$rootScope.global_showSiteFooter = hideSiteFooter.indexOf(toState.name) < 0 ? true : false;
				if (token && toState.name === 'signin') {
					ev.preventDefault();
					$state.go('main.index', {}, {
						location: 'replace'
					});
				}
			});
			$rootScope.$on('event:auth-loginRequired', function (ev, data) { //401	
				$localStorage.$reset();
				$sessionStorage.$reset();
				$rootScope.global_showSignin = true;
			});
			$rootScope.$on('event:auth-forbidden', function (ev, data) { //403
				$rootScope.global_error = {
					status: data.status,
					message: data.data.message
				}
				$state.go('error', {}, {
					location: 'replace'
				});
			});
			$rootScope.$on('event:not-found', function (ev, data) { //404
				$rootScope.global_error = {
					status: data.status,
					message: data.message
				}
				$state.go('error', {}, {
					location: 'replace'
				});
			});
		}
	]);

	//loading  
	app.config(['$httpProvider', function ($httpProvider) {
		var numLoadings = 0;
		$httpProvider.interceptors.push([
			'$q',
			'$rootScope',
			'$localStorage',
			'$sessionStorage',
			function ($q, $rootScope, $localStorage, $sessionStorage) {
				return {
					'request': function (config) {
						if (!config.headers.accessToken) {
							var token = $localStorage.token || $sessionStorage.token;
							config.headers.accessToken = token;
						}
						numLoadings++;
						$rootScope.global_loading = true;
						return config;
					},
					'response': function (response) {
						if (!(--numLoadings)) {
							$rootScope.global_loading = false;
						}
						return response;
					},
					'responseError': function (rejection) {
						if (rejection.status === 404) {
							var data = {
								status: rejection.status,
								message: rejection.data.message
							}
							$rootScope.$broadcast('event:not-found', data);
						}
						numLoadings--;
						$rootScope.global_loading = false;
						return $q.reject(rejection);
					}
				};
			}
		]);
	}]);
}());