(function () {

	'use strict';
	
	var app = angular.module('app.controller', []);

	app.controller('FooterCtrl', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
		$scope.showBugReport = function (ev) {
			$mdDialog.show({
				controller: 'BugReportCtrl',
				templateUrl: '/backend/tpls/dialog_bug_report.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true
			});
		}
	}]);
	app.controller('ToastCtrl', ['$scope', 'ToastService', function ($scope, ToastService) {
		$scope.closeToast = function () {
			ToastService.hide();
		};
	}]);
	app.controller('UploadCtrl', ['$scope', 'Upload', 'ToastService', '$document', function ($scope, Upload, ToastService, $document) {
		function upload(file, url, callback) {
			if (file && !file.$error) {
				Upload.upload({
						url: url,
						file: file
					})
					.progress(function (ev) {
						file.progress = parseInt(100.0 * ev.loaded / ev.total);
					})
					.success(function (data, status, headers, config) {
						callback(data);
					})
					.error(function (data, status, headers, config) {
						callback(data);
					});
			} else if (file && file.$error) {
				if (file.$error === 'maxSize') {
					return ToastService.show($scope, {
						message: '图片最大支持' + file.$errorParam + '!',
						status: 'error',
						canClose: true
					});
				}
			}
		}
		$scope.uploadAvatar = function (file) {
			$scope.f = file;
			var id = $scope.user._id;
			var url = '/duwei/x_upload/avatar/' + id;
			upload(file, url, function (data, status, headers, config) {
				if (data.success) {
					$scope.user.avatar = data.url + '?' + new Date().getTime();
				} else {
					ToastService.show($scope, {
						message: data.message,
						status: 'error',
						canClose: true
					});
				}
			});
		}
		$scope.uploadBlogCover = function (file) {
			$scope.f = file;
			var url = '/duwei/x_upload/postCover';
			upload(file, url, function (data, status, headers, config) {
				if (data.success) {
					$scope.post.cover = data.url;
				} else {
					ToastService.show($scope, {
						message: data.message,
						status: 'error',
						canClose: true
					});
				}
			});
		}
		$scope.uploadSiteCover = function (file) {
			$scope.f = file;
			var url = '/duwei/x_upload/cover';
			upload(file, url, function (data, status, headers, config) {
				if (data.success) {
					$scope.settings.img_cover = data.url + '?' + new Date().getTime();
				} else {
					ToastService.show($scope, {
						message: data.message,
						status: 'error',
						canClose: true
					});
				}
			});
		}
		$scope.uploadBugPic = function (files) {
			$scope.fs = files;
			if (files && files.length) {
				for (var i = 0; i < files.length; i++) {
					var file = files[i];
					var url = '/duwei/x_upload/bug';
					upload(file, url, function (data, status, headers, config) {
						if (data.success) {
							$scope.report.img.push(data.url);
						} else {
							ToastService.show($scope, {
								message: data.message,
								status: 'error',
								canClose: true,
								parent: $document[0].querySelector('#bugReport'),
								position: 'top left'
							});
						}
					});
				}
			}
		}
	}]);
	app.controller('BugReportCtrl', ['$scope', '$http', '$mdDialog', 'ToastService', '$document', function ($scope, $http, $mdDialog, ToastService, $document) {
		$scope.report = {
			subject: '',
			detail: '',
			img: []
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.submit = function () {
			$http.post('/duwei/x_report', $scope.report)
				.success(function (res) {
					ToastService.show($scope, {
						message: res.message,
						status: 'success'
					})
					$mdDialog.cancel();
				})
				.error(function (res) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('#bugReport'),
						position: 'top left'
					});
				});
		}
	}]);
	app.controller('ApplyResetCtrl', ['$scope', '$mdDialog', '$document', 'SignService', 'ToastService', function ($scope, $mdDialog, $document, SignService, ToastService) {
		$scope.apply = {
			email: ''
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.applyReset = function () {
			SignService.applyReset($scope.apply.email, function (res) {
				if (!res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('#applyReset'),
						position: 'top left'
					});
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'success'
					})
					$mdDialog.cancel();
				}
			});
		}
	}]);
	app.controller('ResetPassCtrl', ['$scope', '$document', '$location', '$state', 'ToastService', 'SignService', function ($scope, $document, $location, $state, ToastService, SignService) {
		$scope.NewPass = {
			pass: '',
			rePass: '',
			key: $location.search().key
		}
		$scope.resetPass = function () {
			SignService.resetPass($scope.NewPass, function (res) {
				if (!res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('.sign'),
						position: 'top left'
					});
				} else {
					ToastService.show($scope, {
						message: res.message + '请登录...',
						status: 'success',
						hideDelay: 3000
					});
					$state.go('signin', {}, {
						location: 'replace'
					});
				}
			});
		}
	}]);

	app.controller('SignupCtrl', ['$scope', '$rootScope', '$state', '$document', '$location', 'SignService', 'ToastService', function ($scope, $rootScope, $state, $document, $location, SignService, ToastService) {
		$scope.user = {
			name: '',
			email: '',
			pass: '',
			rePass: '',
			key: $location.search().key
		}
		$scope.signup = function () {
			SignService.signup($scope.user, function (res) {
				if (res.success) {
					ToastService.show($rootScope, {
						message: res.message + ' 请登录..',
						status: 'success',
						hideDelay: 3000
					});
					$state.go('signin', {}, {
						location: 'replace'
					});
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						parent: $document[0].querySelector('.sign'),
						canClose: true,
						position: 'top left'
					});
				}
			});
		}
	}]);
	app.controller('SigninCtrl', ['$scope', '$rootScope', '$state', '$localStorage', '$sessionStorage', '$mdDialog', '$document', 'SignService', 'ToastService', function ($scope, $rootScope, $state, $localStorage, $sessionStorage, $mdDialog, $document, SignService, ToastService) {
		$scope.user = {
			email: '',
			pass: '',
			storage: false
		}
		$scope.showApplyResetDialog = function (ev) {
			$mdDialog.show({
				controller: 'ApplyResetCtrl',
				templateUrl: '/backend/tpls/dialog_apply_reset.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true
			});
		}
		$scope.signin = function () {
			SignService.signin($scope.user);
		}
		$scope.$on('event:auth-loginConfirmed', function (ev, data) {
			var storage = $scope.user.storage ? $localStorage : $sessionStorage;
			storage.token = data.token;
			storage.user = data.user;
			ToastService.show($rootScope, {
				status: 'success',
				message: '登录成功！' + '欢迎 ' + data.user.name,
				hideDelay: 2000
			});
			$rootScope.global_showSignin = false;
			$state.go('main.index', {}, {
				location: 'replace'
			});
		});
		$scope.$on('event:auth-signin-failed', function (ev, data) {
			var toastOption = {
				message: data.message,
				status: data.success ? 'success' : 'error',
				canClose: data.success ? false : true,
				position: 'top left',
				parent: $document[0].querySelector('.sign')
			}
			ToastService.show($scope, toastOption);
		});
	}]);
	app.controller('ErrorCtrl', ['$scope', '$state', function ($scope, $state) {
		$scope.goHome = function () {
			$state.go('main.index', {}, {
				location: 'replace'
			});
		}
	}]);
	app.controller('MainCtrl', ['$scope', '$rootScope', '$state', '$mdSidenav', '$mdDialog', '$mdUtil', 'SignService', function ($scope, $rootScope, $state, $mdSidenav, $mdDialog, $mdUtil, SignService) {
		$scope.menus = [{
			name: '首页',
			icon: 'home',
			ariaLabel: 'goIndexPage',
			state: 'main.index',
			power: 0
		}, {
			name: '新建',
			icon: 'add',
			ariaLabel: 'goEditorPage',
			state: 'main.editor',
			power: 0
		}, {
			name: '列表',
			icon: 'posts',
			ariaLabel: 'goPostListPage',
			state: 'main.posts',
			power: 0
		}, {
			name: '用户',
			icon: 'users',
			ariaLabel: 'goUserListPage',
			state: 'main.users',
			power: 0
		}, {
			name: '邀请',
			icon: 'adduser',
			ariaLabel: 'addPerson',
			power: 1
		}, {
			name: '设置',
			icon: 'settings',
			ariaLabel: 'goSettingsPage',
			state: 'main.settings',
			power: 2
		}];
		$scope.menuClick = function ($index, ev) {
			var clickMenu = $scope.menus[$index];
			$mdSidenav('site-sidebar-left').close();
			if (clickMenu.state) {
				$state.go(clickMenu.state);
			} else {
				$mdDialog.show({
					controller: 'InviteCtrl',
					templateUrl: '/backend/tpls/dialog_invite.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true
				});
			}
		}
		$scope.signout = function () {
			SignService.signout();
		}
		$scope.showSiteSidebar = buildSidebarToggler('site-sidebar-left');

		function buildSidebarToggler(Sidebar) {
			var debounceFn = $mdUtil.debounce(function () {
				$mdSidenav(Sidebar)
					.toggle();
			}, 200);
			return debounceFn;
		}
	}]);
	app.controller('IndexCtrl', ['$scope', 'user', 'settings', function ($scope, user, settings) {
		$scope.posts = user.posts;
		$scope.user = user.user;
		$scope.settings = settings.settings;
		if ($scope.user.avatar) {
			var avatar = $scope.user.avatar.split('.');
			if (avatar[avatar.length - 1] === 'svg') {
				$scope.user.avatar = $scope.user.avatar + '?' + new Date().getTime();
			} else {
				avatar[1] = 'large';
				$scope.user.avatar = avatar.join('.') + '?' + new Date().getTime();
			}
		} else {
			if ($scope.user.sex) {
				$scope.user.avatar = '/backend/i/avatar_man.svg' + '?' + new Date().getTime();
			} else {
				$scope.user.avatar = '/backend/i/avatar_woman.svg' + '?' + new Date().getTime();
			}
		}
	}]);
	app.controller('EditorCtrl', ['$scope', '$rootScope', '$timeout', '$state', '$stateParams', '$mdSidenav', '$mdToast', '$mdDialog', '$mdUtil', 'ToastService', 'post', 'classify', 'PostService', function ($scope, $rootScope, $timeout, $state, $stateParams, $mdSidenav, $mdToast, $mdDialog, $mdUtil, ToastService, post, classify, PostService) {
		var postData = post ? post.post : '';
		var saveDelay = 5000;
		var saveOnOff = true;
		$scope.confirmPass = '';

		(function (postData) {
			$scope.allClassify = classify.classify;
			$scope.allTags = classify.tags;
			$scope.post = {};
			if (postData) {
				for (var attr in postData) {
					if (attr !== '_id') {
						$scope.post[attr] = postData[attr];
					} else {
						$scope.post.id = postData[attr];
					}
				}
				if (postData.tags) {
					getTagName(postData.tags, function (tags) {
						$scope.post.tags = tags;
					});
				}
				if (postData.classify) {
					$scope.post.classify = $scope.post.classify.name;
				}
			} else {
				$scope.post = {
					id: null,
					title: '',
					md: '',
					html: '',
					classify:'',
					tags: [],
					summary: '',
					cover: '',
					published: false
				}
			}
		})(postData);
		
		function getTagName(tags, cb) {
			var newTags = [];
			for (var i = 0; i < tags.length; i++) {
				newTags.push(tags[i].name);
			}
			cb(newTags);
		}

		function showToast(res) {
			var option = {
				status: res.success ? 'success' : 'error',
				message: res.message,
				hideDelay: res.success ? 3000 : 6000,
				canClose: res.success ? false : true
			}
			ToastService.show($scope, option);

			$timeout(function () {
				saveOnOff = true;
			}, saveDelay);

		}

		function updatePost() {
			PostService.updatePost($scope.post, function (res) {
				showToast(res);
			});
		}
		$scope.showPostSettings = buildToggler('post-settings-sidebar');
		$scope.closeSidenav = function () {
			$mdSidenav('post-settings-sidebar').close();
		};
		$scope.addClassify = function (ev) {
			if ($scope.classify) {
				if (ev.keyCode === 13) {
					if ($scope.allClassify.indexOf($scope.classify) < 0) {
						$scope.allClassify.push($scope.classify);
					}
					$scope.classify = '';
				}
			}
		}
		$scope.savePost = function () {
			if (saveOnOff) {
				saveOnOff = false;
				$scope.post.md = $scope.editor.getMarkdown();
				$scope.post.html = $scope.editor.getPreviewedHTML();
				if ($scope.post.id) {
					updatePost();
				} else {
					if (!$scope.post.title) {
						var data = {
							success: 0,
							message: '你必须写标题!'
						}
						showToast(data);
					} else {
						PostService.savePost($scope.post, function (res) {
							if (res.success) {
								$scope.post.id = res.id;
								return showToast(res);
							}
							showToast(res);
						});
					}
				}
			}
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.removePost = function () {
			var query = {
				id: $scope.post.id,
				pass: $scope.confirmPass
			}
			PostService.removePost(query, function (res) {
				if (res.success) {
					ToastService.show($rootScope, {
						status: 'success',
						message: res.message,
						hideDelay: 3000
					});
					$mdDialog.cancel();
					if ($stateParams.id) {
						$state.go('main.posts');
					} else {
						$state.reload();
					}
				} else {
					showToast(res);
				}
			});
		}
		$scope.showRemoveDialog = function (ev) {
			if ($scope.post.author && $scope.global_user.power > $scope.post.author.power) {
				$scope.targetPost = $scope.post;
				$mdDialog.show({
					scope: $scope,
					templateUrl: '/backend/tpls/dialog_confirm_pass_post.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					preserveScope: true
				});
			} else {
				var confirm = $mdDialog.confirm()
					.title('你确定删除 ' + $scope.post.title + ' 这篇文章吗？')
					.content('<p>此操作没有备份！没有后悔药！</p>')
					.ariaLabel('removePostDialog')
					.targetEvent(ev)
					.ok('删除')
					.cancel('我冷静一下');

				$mdDialog.show(confirm).then(function () {
					var query = {
						id: $scope.post.id
					}
					PostService.removePost(query, function (res) {
						if (res.success) {
							ToastService.show($rootScope, {
								status: 'success',
								message: res.message,
								hideDelay: 3000
							});
							if ($stateParams.id) {
								$state.go('main.posts');
							} else {
								$state.reload();
							}
						} else {
							showToast(res);
						}
					});
				}, function () {
					ToastService.show($scope, {
						message: '取消了！'
					});
				});
			}
		}
		$scope.pubPost = function () {
			var data;
			if (!$scope.post.summary) {
				data = {
					success: 0,
					message: '发布前你必须写博文简介!'
				}
				return showToast(data);
			} else if (!$scope.post.classify) {
				data = {
					success: 0,
					message: '发布前你必须写给博文分个类!'
				}
				return showToast(data);
			} else {
				if (saveOnOff) {
					$scope.post.md = $scope.editor.getMarkdown();
					$scope.post.published = true;
					$scope.savePost();
				}
			}
		}
		$scope.unpubPost = function () {
			if (saveOnOff) {
				$scope.post.md = $scope.editor.getMarkdown();
				$scope.post.published = false;
				$scope.savePost();
			}
		}

		function buildToggler(navID) {
			var debounceFn = $mdUtil.debounce(function () {
				$mdSidenav(navID)
					.toggle();
			}, 200);
			return debounceFn;
		}
	}]);
	app.controller('TagCtrl', ['$scope', function ($scope) {
		this.selectedItem = null;
		this.searchText = null;
		this.querySearch = querySearch;
		this.tags = loadTags($scope.allTags);

		function querySearch(query) {
			var results = query ? this.tags.filter(createFilterFor(query)) : [];
			var newResults = [];
			results.forEach(function (val, index) {
				newResults.push(val.name);
			});
			return newResults;
		}

		function createFilterFor(query) {
			var lowercaseQuery;
			try {
				lowercaseQuery = query.toLowerCase();
			} catch (e) {
				lowercaseQuery = query;
			}
			return function filterFn(tag) {
				return (tag._lowername.indexOf(lowercaseQuery) === 0);
			};
		}

		function loadTags(tags) {
			return tags.map(function (tag) {
				try {
					tag._lowername = tag.name.toLowerCase();
				} catch (e) {
					tag._lowername = tag.name;
				}
				return tag;
			});
		}
	}]);
	app.controller('PostsCtrl', ['$scope', '$state', '$document', '$mdDialog', 'PostService', 'ToastService', 'classify', 'author', 'posts', function ($scope, $state, $document, $mdDialog, PostService, ToastService, classify, author, posts) {
		$scope.classify = classify.classify;
		$scope.author = author.users;
		$scope.posts = posts.posts;
		$scope.confirmPass = '';
		$scope.query = {
			author: '',
			classify: '',
			published: ''
		}
		$scope.editPost = function (id) {
			$state.go('main.editor', {
				id: id
			});
		}
		$scope.showRemoveDialog = function (post, ev, index) {
			if ($scope.global_user.power > post.author.power) {
				$scope.targetPost = post;
				$mdDialog.show({
					scope: $scope,
					templateUrl: '/backend/tpls/dialog_confirm_pass_post.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					preserveScope: true
				});
			} else {
				var confirm = $mdDialog.confirm()
					.title('你确定删除 ' + post.title + ' 这篇文章吗？')
					.content('<p>此操作没有备份！没有后悔药！</p>')
					.ariaLabel('removePostDialog')
					.targetEvent(ev)
					.ok('删除')
					.cancel('我冷静一下');

				var query = {
					id: post._id
				}
				$mdDialog.show(confirm).then(function () {
					PostService.removePost(query, function (res) {
						if (res.success) {
							ToastService.show($scope, {
								status: 'success',
								message: res.message,
								hideDelay: 3000
							});
							$mdDialog.cancel();
							$scope.posts.splice(index, 1);
						} else {
							ToastService.show($scope, {
								status: 'error',
								message: res.message,
								hideDelay: 6000,
								canClose: true
							});
						}
					});
				}, function () {
					ToastService.show($scope, {
						message: '取消了！'
					});
				});
			}
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.removePost = function () {
			var query = {
				id: $scope.targetPost._id,
				pass: $scope.confirmPass
			}
			PostService.removePost(query, function (res) {
				if (res.success) {
					ToastService.show($scope, {
						status: 'success',
						message: res.message,
						hideDelay: 3000
					});
					$mdDialog.cancel();
					$state.reload();
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						parent: $document[0].querySelector('#confirmPass'),
						canClose: true,
						position: 'top left'
					});
				}
			});
		}
		$scope.getPosts = function () {
			var query = {};
			for (var attr in $scope.query) {
				if ($scope.query[attr] || typeof $scope.query[attr] === 'boolean') {
					query[attr] = $scope.query[attr];
				}
			}
			PostService.getPosts(query)
				.then(function (result) {
					$scope.posts = result.posts;
				});
		}
	}]);
	app.controller('UsersCtrl', ['$scope', '$document', '$state', '$mdDialog', 'users', 'UserService', 'ToastService', function ($scope, $document, $state, $mdDialog, users, UserService, ToastService) {
		$scope.users = users.users;
		$scope.confirmPass = '';
		$scope.targetUser = {};
		$scope.goEditPage = function (id) {
			$state.go('main.user', {
				id: id
			});
		}
		$scope.showRemoveDialog = function (user, ev) {
			$scope.targetUser = user;
			$mdDialog.show({
				scope: $scope,
				templateUrl: '/backend/tpls/dialog_confirm_pass.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				preserveScope: true
			});
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.removeUser = function () {
			UserService.removeUser($scope.targetUser._id, $scope.confirmPass, function (res) {
				if (res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'success',
						hideDelay: '3000'
					});
					$mdDialog.cancel();
					$state.reload();
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						parent: $document[0].querySelector('#confirmPass'),
						canClose: true,
						position: 'top left'
					});
				}
			});
		}
	}]);
	app.controller('UserCtrl', ['$scope', 'user', 'UserService', 'ToastService', '$localStorage', '$sessionStorage', function ($scope, user, UserService, ToastService, $localStorage, $sessionStorage) {
		$scope.user = user.user;
		if (!$scope.user.avatar) {
			if ($scope.user.sex) {
				$scope.user.avatar = '/backend/i/avatar_man.svg';
			} else {
				$scope.user.avatar = '/backend/i/avatar_woman.svg';
			}
		}
		if (typeof $scope.user.sex === 'boolean') {
			$scope.user.sex = $scope.user.sex.toString();
		}
		$scope.updateUser = function () {
			var updateData = {};

			function checkEmptyObj(Obj) {
				for (var attr in Obj) {
					return false;
				}
				return true;
			}
			for (var attr in $scope.user) {
				if ($scope.edit_user_form[attr]) {
					if ($scope.edit_user_form[attr].$dirty && $scope.edit_user_form[attr].$valid) {
						if ($scope.user[attr]) {
							updateData[attr] = $scope.user[attr];
						}
					}
				}
			}
			if (!checkEmptyObj(updateData)) {
				UserService.updateUser($scope.user._id, updateData, function (res) {
					if (res.success) {
						if ($scope.user._id.toString() === $scope.global_user._id.toString()) {
							var storage = $localStorage.user ? $localStorage : $sessionStorage;
							storage.user = {
								_id: $scope.user._id,
								name: $scope.user.name,
								power: $scope.user.power
							}
						}
						ToastService.show($scope, {
							message: res.message,
							status: 'success',
							hideDelay: '3000',
						});
					} else {
						ToastService.show($scope, {
							message: res.message,
							status: 'error',
							canClose: true
						});
					}
				});
			}
		}
	}]);
	app.controller('InviteCtrl', ['$scope', '$mdDialog', '$document', 'UserService', 'ToastService', '$localStorage', '$sessionStorage', function ($scope, $mdDialog, $document, UserService, ToastService, $localStorage, $sessionStorage) {
		$scope.invite = {
			email: '',
			power: 0
		}
		$scope.global_user = $localStorage.user || $sessionStorage.user;
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.inviteUser = function () {
			UserService.addUser($scope.invite, function (res) {
				if (!res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('#inviteUser'),
						position: 'top left'
					});
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'success'
					})
					$mdDialog.cancel();
				}
			});
		}
	}]);

	app.controller('SettingsCtrl', ['$scope', '$http', '$localStorage', '$sessionStorage', 'SettingsService', 'ToastService', 'settings', function ($scope, $http, $localStorage, $sessionStorage, SettingsService, ToastService, settings) {
		var settings = settings.settings;
		$scope.token = $localStorage.token || $sessionStorage.token || $http.defaults.headers.common.accessToken;
		if (settings) {
			$scope.settings = {
				name: settings.name,
				show_num: settings.show_num,
				notice: settings.notice,
				summary: settings.summary,
				img_cover: settings.img_cover
			}
		} else {
			$scope.settings = {
				name: '',
				show_num: 5,
				notice: '',
				summary: '',
				img_cover: ''
			}
		}
		$scope.saveSettings = function () {
			SettingsService.saveSettings($scope.settings, function (result) {
				var status = result.success ? 'success' : 'error';
				var canClose = result.success ? false : true;
				ToastService.show($scope, {
					status: status,
					message: result.message,
					canClose: canClose,
					position: 'bottom right',
					hideDelay: 3000
				});
			});
		}
	}]);
	
}());