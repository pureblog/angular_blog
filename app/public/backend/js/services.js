(function () {

	'use strict';
	var app = angular.module('app.service',[]);
	app.factory('SignService', ['$http','$rootScope','authService','$state','$localStorage','$sessionStorage',function( $http,$rootScope,authService,$state,$localStorage,$sessionStorage){
		return {
			signin : function(user){
				$http.post('/duwei/x_signin',user)
				.success(function(data, status, headers, config){
					$http.defaults.headers.common.accessToken = data.token;
					authService.loginConfirmed(data, function(config) {  
						config.headers.accessToken = data.token;
						return config;
					});
				})
				.error(function(data, status, headers, config){
					$rootScope.$broadcast('event:auth-signin-failed', data);
				});
			},
			signout : function(){
				delete $http.defaults.headers.common.accessToken;
				$localStorage.$reset();
				$sessionStorage.$reset();	
				$state.go('signin',{},{ location: 'replace' });
			},
			signinCancelled : function(){
				authService.loginCancelled();
			},
			checkSignup: function(key){
				return $http.get('/duwei/x_signup',{
					params : key
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});	
			},
			signup: function(user,callback){
				$http.post('/duwei/x_signup',user)
				.success(callback)
				.error(callback);
			},
			checkReset: function(key){
				return $http.get('/duwei/x_reset',{
					params : key
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			applyReset:function(email,callback){
				$http.post('/duwei/x_applyreset',{
					email : email
				})
				.success(callback)
				.error(callback);
			},
			resetPass:function(data,callback){
				$http.post('/duwei/x_reset',data)
				.success(callback)
				.error(callback);
			}
		}
	}]);
	app.factory('ToastService', ['$mdToast', function($mdToast){
		return {
			show:function(scope,options){
				var toastOptions = {
					scope:scope,
					controller: 'ToastCtrl',
					templateUrl:'/backend/tpls/toast.html',
					parent:angular.element(document.body) ,
					preserveScope:true,
					hideDelay:6000,
					position:'top right'
				}
				if(options){
					for(var attr in toastOptions){
						if(options[attr]){
							toastOptions[attr] = options[attr];
						}
					}
					var message = options.message ? options.message : '';
					var status = options.status ? options.status : 'default';
					var canClose = options.canClose ? options.canClose : false;
					scope.toast = {
						message : message,
						status :  status,
						canClose: canClose
					}
					$mdToast.hide();
					if(message){
						$mdToast.show(toastOptions);
					}
				}
			},
			hide:function(){
				$mdToast.hide();
			}
		}
	}])
	app.factory('PostService', ['$http','authService', function($http){
		return {
			savePost : function(post,callback){
				$http.post('/duwei/x_posts',post)
				.success(callback)
				.error(callback);
			},
			removePost : function(query,callback){
				$http.delete('/duwei/x_posts',{
					params:query
				})
				.success(callback)
				.error(callback);		
			},
			updatePost : function(post,callback){
				$http.put('/duwei/x_posts/'+post.id,post)
				.success(callback)
				.error(callback);
			},
			getPosts : function(query){
				query = query || '';
				return $http.get('/duwei/x_posts',{
					params:query
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			getOnePost : function(id){
				if(id){
					return $http.get('/duwei/x_posts/'+id)
					.then(function(res){
						return res.data;
					},function(res){
						return res.data;
					});
				}else{
					return '';
				}
			},
			getClassify : function(){
				return $http.get('/duwei/x_classify')
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			}
		}
	}]);
	app.factory('UserService', ['$rootScope','$http',function($rootScope,$http){
		return {
			getUsers : function(query){
				return $http.get('/duwei/x_admin',{
					params:query
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			getOneUser : function(id){
				if(id){
					return $http.get('/duwei/x_admin/'+id)
					.then(function(res){
						return res.data;
					},function(res){
						return res.data;
					});	
				}
			},
			addUser : function(data,callback){
				$http.post('/duwei/x_invite',data)
				.success(callback)
				.error(callback);
			},
			updateUser :function(id,data,callback){
				$http.put('/duwei/x_admin/'+id,data)
				.success(callback)
				.error(callback);
			},
			removeUser : function(id,pass,callback){
				$http.delete('/duwei/x_admin/'+id+'/'+pass)
				.success(callback)
				.error(callback);
			}
		}
	}]);
	app.factory('SettingsService', ['$http', function($http){
		return {
			getSettings : function(){
				return $http.get('/duwei/x_setting')
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			saveSettings : function(data,callback){
				$http.post('/duwei/x_setting',data)
				.success(callback)
				.error(callback);
			}
		}
	}]);
}());