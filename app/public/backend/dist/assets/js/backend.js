(function () {

	'use strict';
	
	var app = angular.module('Backend', [
		'ui.router',
		'oc.lazyLoad',
		'http-auth-interceptor',
		'ngStorage',
		'ngFileUpload',
		'ngMaterial',
		'ngMessages',
		'app.controller',
		'app.service',
		'app.directive',
		'app.filter'
	]);
	app.config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {
		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
		$stateProvider
			.state('main', {
				abstract: true,
				url: '/duwei',
				templateUrl: '/backend/pages/layout.html',
			})
			.state('error', {
				url: '/duwei/error',
				templateUrl: '/backend/pages/error.html',
				controller: 'ErrorCtrl'
			})
			.state('signin', {
				url: '/duwei/signin',
				templateUrl: '/backend/pages/signin.html'
			})
			.state('signup', {
				url: '/duwei/signup',
				templateUrl: '/backend/pages/signup.html',
				controller: 'SignupCtrl',
				resolve: {
					checkKey: ['SignService', '$location', function (SignService, $location) {
						return SignService.checkSignup($location.search());
					}]
				}
			})
			.state('resetpass', {
				url: '/duwei/resetpass',
				templateUrl: '/backend/pages/reset_pass.html',
				controller: 'ResetPassCtrl',
				resolve: {
					checkKey: ['SignService', '$location', function (SignService, $location) {
						return SignService.checkReset($location.search());
					}]
				}
			})
			.state('main.index', {
				url: '/index',
				templateUrl: '/backend/pages/index.html',
				controller: 'IndexCtrl',
				resolve: {
					'user': ['UserService', '$rootScope', '$localStorage', '$sessionStorage', function (UserService, $rootScope, $localStorage, $sessionStorage) {
						var id;
						if ($localStorage.user) {
							id = $localStorage.user._id;
							return UserService.getOneUser(id);
						} else if ($sessionStorage.user) {
							id = $sessionStorage.user._id;
							return UserService.getOneUser(id);
						} else {
							return $rootScope.$broadcast('event:auth-loginRequired');
						}
						return UserService.getOneUser(id);
					}],
					'settings': ['SettingsService', function (SettingsService) {
						return SettingsService.getSettings();
					}]
				}
			})
			.state('main.editor', {
				url: '/editor/:id',
				templateUrl: '/backend/pages/editor.html',
				controller: 'EditorCtrl',
				resolve: {
					loadcss: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('/lib/editormd/css/editormd.min.css');
					}],
					loadJS: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load({
							serie: true,
							files: [
								'//cdn.bootcss.com/jquery/2.1.4/jquery.min.js',
								'/lib/editormd/editormd.min.js'
							]
						});
					}],
					post: ['PostService', '$stateParams', function (PostService, $stateParams) {
						var id = $stateParams.id || '';
						return PostService.getOnePost(id);
					}],
					'classify': ['PostService', function (PostService) {
						return PostService.getClassify();
					}]
				}
			})
			.state('main.posts', {
				url: '/posts',
				templateUrl: '/backend/pages/posts.html',
				controller: 'PostsCtrl',
				resolve: {
					'classify': ['PostService', function (PostService) {
						return PostService.getClassify();
					}],
					'author': ['UserService', function (UserService) {
						return UserService.getUsers({
							'active': true
						});
					}],
					'posts': ['PostService', function (PostService) {
						return PostService.getPosts();
					}]
				}
			})
			.state('main.users', {
				url: '/users',
				templateUrl: '/backend/pages/users.html',
				controller: 'UsersCtrl',
				resolve: {
					'users': ['UserService', function (UserService) {
						return UserService.getUsers();
					}]
				}
			})
			.state('main.user', {
				url: '/user/:id',
				templateUrl: '/backend/pages/edit_user.html',
				controller: 'UserCtrl',
				resolve: {
					'user': ['UserService', '$stateParams', function (UserService, $stateParams) {
						var id = $stateParams.id;
						return UserService.getOneUser(id);
					}]
				}
			})
			.state('main.settings', {
				url: '/settings',
				templateUrl: '/backend/pages/settings.html',
				controller: 'SettingsCtrl',
				resolve: {
					'settings': ['SettingsService', function (SettingsService) {
						return SettingsService.getSettings();
					}]
				}
			});
		$urlRouterProvider
			.when('/duwei', '/duwei/index')
			.when('/duwei/', '/duwei/index')
			.when('/duwei/editor', '/duwei/editor/')
			.otherwise('/duwei/error');
	}]);
	app.config(['$mdIconProvider',function ($mdIconProvider) {
		$mdIconProvider
			.icon('home', '/fonts/basic_home.svg')
			.icon('add', '/fonts/arrows_plus.svg')
			.icon('posts', '/fonts/basic_folder_multiple.svg')
			.icon('users', '/fonts/basic_postcard_multiple.svg')
			.icon('adduser', '/fonts/basic_elaboration_browser_plus.svg')
			.icon('settings', '/fonts/basic_gear.svg')
			.icon('lock', '/fonts/basic_lock.svg')
			.icon('enter', '/fonts/basic_paperplane.svg')
			.icon('close', '/fonts/arrows_remove.svg')
			.icon('mail', '/fonts/basic_mail.svg')
			.icon('qq', '/fonts/qq.svg')
			.icon('phone', '/fonts/basic_smartphone.svg')
			.icon('yes', '/fonts/arrows_check.svg')
			.icon('up', '/fonts/arrows_up.svg')
			.icon('right', '/fonts/arrows_right.svg')
			.icon('upload', '/fonts/basic_elaboration_cloud_upload.svg')
			.icon('delete', '/fonts/basic_trashcan.svg')
			.icon('send', '/fonts/basic_elaboration_cloud_check.svg')
			.icon('save', '/fonts/basic_elaboration_calendar_check.svg')
			.icon('undo', '/fonts/arrows_anticlockwise.svg')
			.icon('menu', '/fonts/arrows_hamburger1.svg')
			.icon('info', '/fonts/basic_info.svg')
			.icon('link', '/fonts/basic_link.svg')
			.icon('img', '/fonts/basic_picture.svg')
			.icon('tag', '/fonts/ecommerce_sale.svg')
			.icon('edit', '/fonts/software_pencil.svg')
			.icon('date', '/fonts/basic_calendar.svg')
			.icon('refresh', '/fonts/arrows_rotate.svg')
			.icon('male', '/fonts/basic_male.svg')
			.icon('female', '/fonts/basic_female.svg');
	}]);
	app.run(['$http', '$templateCache', function ($http, $templateCache) {
		var urls = [
			'/fonts/basic_home.svg',
			'/fonts/arrows_plus.svg',
			'/fonts/basic_folder_multiple.svg',
			'/fonts/basic_postcard_multiple.svg',
			'/fonts/basic_elaboration_browser_plus.svg',
			'/fonts/basic_gear.svg',
			'/fonts/basic_lock.svg',
			'/fonts/basic_paperplane.svg',
			'/fonts/arrows_remove.svg',
			'/fonts/basic_mail.svg',
			'/fonts/qq.svg',
			'/fonts/basic_smartphone.svg',
			'/fonts/arrows_check.svg',
			'/fonts/arrows_up.svg',
			'/fonts/basic_elaboration_cloud_upload.svg',
			'/fonts/basic_trashcan.svg',
			'/fonts/basic_elaboration_cloud_check.svg',
			'/fonts/basic_elaboration_calendar_check.svg',
			'/fonts/arrows_anticlockwise.svg',
			'/fonts/arrows_hamburger1.svg',
			'/fonts/basic_info.svg',
			'/fonts/basic_link.svg',
			'/fonts/basic_picture.svg',
			'/fonts/ecommerce_sale.svg',
			'/fonts/software_pencil.svg',
			'/fonts/basic_calendar.svg',
			'/fonts/arrows_rotate.svg',
			'/fonts/basic_male.svg',
			'/fonts/basic_female.svg',
		];
		angular.forEach(urls, function (url) {
			$http.get(url, {
				cache: $templateCache
			});
		});
	}]);
	app.run([
		'$rootScope',
		'$state',
		'$http',
		'SignService',
		'ToastService',
		'$localStorage',
		'$sessionStorage',
		function ($rootScope, $state, $http, SignService, ToastService, $localStorage, $sessionStorage) {
			$rootScope.$on('$stateChangeStart', function (ev, toState, toParams, fromState, fromParams) {
				$rootScope.global_user = $localStorage.user || $sessionStorage.user || '';
				var hideSiteheader = ['main.editor']; //需要隐藏 header 的 路由状态
				var hideSiteFooter = ['main.editor', 'main.settings']; //需要隐藏 footer 的 路由状态
				var token = $localStorage.token || $sessionStorage.token || $http.defaults.headers.common.accessToken;
				$rootScope.global_showSiteheader = hideSiteheader.indexOf(toState.name) < 0 ? true : false;
				$rootScope.global_showSiteFooter = hideSiteFooter.indexOf(toState.name) < 0 ? true : false;
				if (token && toState.name === 'signin') {
					ev.preventDefault();
					$state.go('main.index', {}, {
						location: 'replace'
					});
				}
			});
			$rootScope.$on('event:auth-loginRequired', function (ev, data) { //401	
				$localStorage.$reset();
				$sessionStorage.$reset();
				$rootScope.global_showSignin = true;
			});
			$rootScope.$on('event:auth-forbidden', function (ev, data) { //403
				$rootScope.global_error = {
					status: data.status,
					message: data.data.message
				}
				$state.go('error', {}, {
					location: 'replace'
				});
			});
			$rootScope.$on('event:not-found', function (ev, data) { //404
				$rootScope.global_error = {
					status: data.status,
					message: data.message
				}
				$state.go('error', {}, {
					location: 'replace'
				});
			});
		}
	]);

	//loading  
	app.config(['$httpProvider', function ($httpProvider) {
		var numLoadings = 0;
		$httpProvider.interceptors.push([
			'$q',
			'$rootScope',
			'$localStorage',
			'$sessionStorage',
			function ($q, $rootScope, $localStorage, $sessionStorage) {
				return {
					'request': function (config) {
						if (!config.headers.accessToken) {
							var token = $localStorage.token || $sessionStorage.token;
							config.headers.accessToken = token;
						}
						numLoadings++;
						$rootScope.global_loading = true;
						return config;
					},
					'response': function (response) {
						if (!(--numLoadings)) {
							$rootScope.global_loading = false;
						}
						return response;
					},
					'responseError': function (rejection) {
						if (rejection.status === 404) {
							var data = {
								status: rejection.status,
								message: rejection.data.message
							}
							$rootScope.$broadcast('event:not-found', data);
						}
						numLoadings--;
						$rootScope.global_loading = false;
						return $q.reject(rejection);
					}
				};
			}
		]);
	}]);
}());
(function () {

	'use strict';
	
	var app = angular.module('app.controller', []);

	app.controller('FooterCtrl', ['$scope', '$mdDialog', function ($scope, $mdDialog) {
		$scope.showBugReport = function (ev) {
			$mdDialog.show({
				controller: 'BugReportCtrl',
				templateUrl: '/backend/tpls/dialog_bug_report.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true
			});
		}
	}]);
	app.controller('ToastCtrl', ['$scope', 'ToastService', function ($scope, ToastService) {
		$scope.closeToast = function () {
			ToastService.hide();
		};
	}]);
	app.controller('UploadCtrl', ['$scope', 'Upload', 'ToastService', '$document', function ($scope, Upload, ToastService, $document) {
		function upload(file, url, callback) {
			if (file && !file.$error) {
				Upload.upload({
						url: url,
						file: file
					})
					.progress(function (ev) {
						file.progress = parseInt(100.0 * ev.loaded / ev.total);
					})
					.success(function (data, status, headers, config) {
						callback(data);
					})
					.error(function (data, status, headers, config) {
						callback(data);
					});
			} else if (file && file.$error) {
				if (file.$error === 'maxSize') {
					return ToastService.show($scope, {
						message: '图片最大支持' + file.$errorParam + '!',
						status: 'error',
						canClose: true
					});
				}
			}
		}
		$scope.uploadAvatar = function (file) {
			$scope.f = file;
			var id = $scope.user._id;
			var url = '/duwei/x_upload/avatar/' + id;
			upload(file, url, function (data, status, headers, config) {
				if (data.success) {
					$scope.user.avatar = data.url + '?' + new Date().getTime();
				} else {
					ToastService.show($scope, {
						message: data.message,
						status: 'error',
						canClose: true
					});
				}
			});
		}
		$scope.uploadBlogCover = function (file) {
			$scope.f = file;
			var url = '/duwei/x_upload/postCover';
			upload(file, url, function (data, status, headers, config) {
				if (data.success) {
					$scope.post.cover = data.url;
				} else {
					ToastService.show($scope, {
						message: data.message,
						status: 'error',
						canClose: true
					});
				}
			});
		}
		$scope.uploadSiteCover = function (file) {
			$scope.f = file;
			var url = '/duwei/x_upload/cover';
			upload(file, url, function (data, status, headers, config) {
				if (data.success) {
					$scope.settings.img_cover = data.url + '?' + new Date().getTime();
				} else {
					ToastService.show($scope, {
						message: data.message,
						status: 'error',
						canClose: true
					});
				}
			});
		}
		$scope.uploadBugPic = function (files) {
			$scope.fs = files;
			if (files && files.length) {
				for (var i = 0; i < files.length; i++) {
					var file = files[i];
					var url = '/duwei/x_upload/bug';
					upload(file, url, function (data, status, headers, config) {
						if (data.success) {
							$scope.report.img.push(data.url);
						} else {
							ToastService.show($scope, {
								message: data.message,
								status: 'error',
								canClose: true,
								parent: $document[0].querySelector('#bugReport'),
								position: 'top left'
							});
						}
					});
				}
			}
		}
	}]);
	app.controller('BugReportCtrl', ['$scope', '$http', '$mdDialog', 'ToastService', '$document', function ($scope, $http, $mdDialog, ToastService, $document) {
		$scope.report = {
			subject: '',
			detail: '',
			img: []
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.submit = function () {
			$http.post('/duwei/x_report', $scope.report)
				.success(function (res) {
					ToastService.show($scope, {
						message: res.message,
						status: 'success'
					})
					$mdDialog.cancel();
				})
				.error(function (res) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('#bugReport'),
						position: 'top left'
					});
				});
		}
	}]);
	app.controller('ApplyResetCtrl', ['$scope', '$mdDialog', '$document', 'SignService', 'ToastService', function ($scope, $mdDialog, $document, SignService, ToastService) {
		$scope.apply = {
			email: ''
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.applyReset = function () {
			SignService.applyReset($scope.apply.email, function (res) {
				if (!res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('#applyReset'),
						position: 'top left'
					});
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'success'
					})
					$mdDialog.cancel();
				}
			});
		}
	}]);
	app.controller('ResetPassCtrl', ['$scope', '$document', '$location', '$state', 'ToastService', 'SignService', function ($scope, $document, $location, $state, ToastService, SignService) {
		$scope.NewPass = {
			pass: '',
			rePass: '',
			key: $location.search().key
		}
		$scope.resetPass = function () {
			SignService.resetPass($scope.NewPass, function (res) {
				if (!res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('.sign'),
						position: 'top left'
					});
				} else {
					ToastService.show($scope, {
						message: res.message + '请登录...',
						status: 'success',
						hideDelay: 3000
					});
					$state.go('signin', {}, {
						location: 'replace'
					});
				}
			});
		}
	}]);

	app.controller('SignupCtrl', ['$scope', '$rootScope', '$state', '$document', '$location', 'SignService', 'ToastService', function ($scope, $rootScope, $state, $document, $location, SignService, ToastService) {
		$scope.user = {
			name: '',
			email: '',
			pass: '',
			rePass: '',
			key: $location.search().key
		}
		$scope.signup = function () {
			SignService.signup($scope.user, function (res) {
				if (res.success) {
					ToastService.show($rootScope, {
						message: res.message + ' 请登录..',
						status: 'success',
						hideDelay: 3000
					});
					$state.go('signin', {}, {
						location: 'replace'
					});
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						parent: $document[0].querySelector('.sign'),
						canClose: true,
						position: 'top left'
					});
				}
			});
		}
	}]);
	app.controller('SigninCtrl', ['$scope', '$rootScope', '$state', '$localStorage', '$sessionStorage', '$mdDialog', '$document', 'SignService', 'ToastService', function ($scope, $rootScope, $state, $localStorage, $sessionStorage, $mdDialog, $document, SignService, ToastService) {
		$scope.user = {
			email: '',
			pass: '',
			storage: false
		}
		$scope.showApplyResetDialog = function (ev) {
			$mdDialog.show({
				controller: 'ApplyResetCtrl',
				templateUrl: '/backend/tpls/dialog_apply_reset.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true
			});
		}
		$scope.signin = function () {
			SignService.signin($scope.user);
		}
		$scope.$on('event:auth-loginConfirmed', function (ev, data) {
			var storage = $scope.user.storage ? $localStorage : $sessionStorage;
			storage.token = data.token;
			storage.user = data.user;
			ToastService.show($rootScope, {
				status: 'success',
				message: '登录成功！' + '欢迎 ' + data.user.name,
				hideDelay: 2000
			});
			$rootScope.global_showSignin = false;
			$state.go('main.index', {}, {
				location: 'replace'
			});
		});
		$scope.$on('event:auth-signin-failed', function (ev, data) {
			var toastOption = {
				message: data.message,
				status: data.success ? 'success' : 'error',
				canClose: data.success ? false : true,
				position: 'top left',
				parent: $document[0].querySelector('.sign')
			}
			ToastService.show($scope, toastOption);
		});
	}]);
	app.controller('ErrorCtrl', ['$scope', '$state', function ($scope, $state) {
		$scope.goHome = function () {
			$state.go('main.index', {}, {
				location: 'replace'
			});
		}
	}]);
	app.controller('MainCtrl', ['$scope', '$rootScope', '$state', '$mdSidenav', '$mdDialog', '$mdUtil', 'SignService', function ($scope, $rootScope, $state, $mdSidenav, $mdDialog, $mdUtil, SignService) {
		$scope.menus = [{
			name: '首页',
			icon: 'home',
			ariaLabel: 'goIndexPage',
			state: 'main.index',
			power: 0
		}, {
			name: '新建',
			icon: 'add',
			ariaLabel: 'goEditorPage',
			state: 'main.editor',
			power: 0
		}, {
			name: '列表',
			icon: 'posts',
			ariaLabel: 'goPostListPage',
			state: 'main.posts',
			power: 0
		}, {
			name: '用户',
			icon: 'users',
			ariaLabel: 'goUserListPage',
			state: 'main.users',
			power: 0
		}, {
			name: '邀请',
			icon: 'adduser',
			ariaLabel: 'addPerson',
			power: 1
		}, {
			name: '设置',
			icon: 'settings',
			ariaLabel: 'goSettingsPage',
			state: 'main.settings',
			power: 2
		}];
		$scope.menuClick = function ($index, ev) {
			var clickMenu = $scope.menus[$index];
			$mdSidenav('site-sidebar-left').close();
			if (clickMenu.state) {
				$state.go(clickMenu.state);
			} else {
				$mdDialog.show({
					controller: 'InviteCtrl',
					templateUrl: '/backend/tpls/dialog_invite.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true
				});
			}
		}
		$scope.signout = function () {
			SignService.signout();
		}
		$scope.showSiteSidebar = buildSidebarToggler('site-sidebar-left');

		function buildSidebarToggler(Sidebar) {
			var debounceFn = $mdUtil.debounce(function () {
				$mdSidenav(Sidebar)
					.toggle();
			}, 200);
			return debounceFn;
		}
	}]);
	app.controller('IndexCtrl', ['$scope', 'user', 'settings', function ($scope, user, settings) {
		$scope.posts = user.posts;
		$scope.user = user.user;
		$scope.settings = settings.settings;
		if ($scope.user.avatar) {
			var avatar = $scope.user.avatar.split('.');
			if (avatar[avatar.length - 1] === 'svg') {
				$scope.user.avatar = $scope.user.avatar + '?' + new Date().getTime();
			} else {
				avatar[1] = 'large';
				$scope.user.avatar = avatar.join('.') + '?' + new Date().getTime();
			}
		} else {
			if ($scope.user.sex) {
				$scope.user.avatar = '/backend/i/avatar_man.svg' + '?' + new Date().getTime();
			} else {
				$scope.user.avatar = '/backend/i/avatar_woman.svg' + '?' + new Date().getTime();
			}
		}
	}]);
	app.controller('EditorCtrl', ['$scope', '$rootScope', '$timeout', '$state', '$stateParams', '$mdSidenav', '$mdToast', '$mdDialog', '$mdUtil', 'ToastService', 'post', 'classify', 'PostService', function ($scope, $rootScope, $timeout, $state, $stateParams, $mdSidenav, $mdToast, $mdDialog, $mdUtil, ToastService, post, classify, PostService) {
		var postData = post ? post.post : '';
		var saveDelay = 5000;
		var saveOnOff = true;
		$scope.confirmPass = '';

		(function (postData) {
			$scope.allClassify = classify.classify;
			$scope.allTags = classify.tags;
			$scope.post = {};
			if (postData) {
				for (var attr in postData) {
					if (attr !== '_id') {
						$scope.post[attr] = postData[attr];
					} else {
						$scope.post.id = postData[attr];
					}
				}
				if (postData.tags) {
					getTagName(postData.tags, function (tags) {
						$scope.post.tags = tags;
					});
				}
				if (postData.classify) {
					$scope.post.classify = $scope.post.classify.name;
				}
			} else {
				$scope.post = {
					id: null,
					title: '',
					md: '',
					html: '',
					classify:'',
					tags: [],
					summary: '',
					cover: '',
					published: false
				}
			}
		})(postData);
		
		function getTagName(tags, cb) {
			var newTags = [];
			for (var i = 0; i < tags.length; i++) {
				newTags.push(tags[i].name);
			}
			cb(newTags);
		}

		function showToast(res) {
			var option = {
				status: res.success ? 'success' : 'error',
				message: res.message,
				hideDelay: res.success ? 3000 : 6000,
				canClose: res.success ? false : true
			}
			ToastService.show($scope, option);

			$timeout(function () {
				saveOnOff = true;
			}, saveDelay);

		}

		function updatePost() {
			PostService.updatePost($scope.post, function (res) {
				showToast(res);
			});
		}
		$scope.showPostSettings = buildToggler('post-settings-sidebar');
		$scope.closeSidenav = function () {
			$mdSidenav('post-settings-sidebar').close();
		};
		$scope.addClassify = function (ev) {
			if ($scope.classify) {
				if (ev.keyCode === 13) {
					if ($scope.allClassify.indexOf($scope.classify) < 0) {
						$scope.allClassify.push($scope.classify);
					}
					$scope.classify = '';
				}
			}
		}
		$scope.savePost = function () {
			if (saveOnOff) {
				saveOnOff = false;
				$scope.post.md = $scope.editor.getMarkdown();
				$scope.post.html = $scope.editor.getPreviewedHTML();
				if ($scope.post.id) {
					updatePost();
				} else {
					if (!$scope.post.title) {
						var data = {
							success: 0,
							message: '你必须写标题!'
						}
						showToast(data);
					} else {
						PostService.savePost($scope.post, function (res) {
							if (res.success) {
								$scope.post.id = res.id;
								return showToast(res);
							}
							showToast(res);
						});
					}
				}
			}
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.removePost = function () {
			var query = {
				id: $scope.post.id,
				pass: $scope.confirmPass
			}
			PostService.removePost(query, function (res) {
				if (res.success) {
					ToastService.show($rootScope, {
						status: 'success',
						message: res.message,
						hideDelay: 3000
					});
					$mdDialog.cancel();
					if ($stateParams.id) {
						$state.go('main.posts');
					} else {
						$state.reload();
					}
				} else {
					showToast(res);
				}
			});
		}
		$scope.showRemoveDialog = function (ev) {
			if ($scope.post.author && $scope.global_user.power > $scope.post.author.power) {
				$scope.targetPost = $scope.post;
				$mdDialog.show({
					scope: $scope,
					templateUrl: '/backend/tpls/dialog_confirm_pass_post.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					preserveScope: true
				});
			} else {
				var confirm = $mdDialog.confirm()
					.title('你确定删除 ' + $scope.post.title + ' 这篇文章吗？')
					.content('<p>此操作没有备份！没有后悔药！</p>')
					.ariaLabel('removePostDialog')
					.targetEvent(ev)
					.ok('删除')
					.cancel('我冷静一下');

				$mdDialog.show(confirm).then(function () {
					var query = {
						id: $scope.post.id
					}
					PostService.removePost(query, function (res) {
						if (res.success) {
							ToastService.show($rootScope, {
								status: 'success',
								message: res.message,
								hideDelay: 3000
							});
							if ($stateParams.id) {
								$state.go('main.posts');
							} else {
								$state.reload();
							}
						} else {
							showToast(res);
						}
					});
				}, function () {
					ToastService.show($scope, {
						message: '取消了！'
					});
				});
			}
		}
		$scope.pubPost = function () {
			var data;
			if (!$scope.post.summary) {
				data = {
					success: 0,
					message: '发布前你必须写博文简介!'
				}
				return showToast(data);
			} else if (!$scope.post.classify) {
				data = {
					success: 0,
					message: '发布前你必须写给博文分个类!'
				}
				return showToast(data);
			} else {
				if (saveOnOff) {
					$scope.post.md = $scope.editor.getMarkdown();
					$scope.post.published = true;
					$scope.savePost();
				}
			}
		}
		$scope.unpubPost = function () {
			if (saveOnOff) {
				$scope.post.md = $scope.editor.getMarkdown();
				$scope.post.published = false;
				$scope.savePost();
			}
		}

		function buildToggler(navID) {
			var debounceFn = $mdUtil.debounce(function () {
				$mdSidenav(navID)
					.toggle();
			}, 200);
			return debounceFn;
		}
	}]);
	app.controller('TagCtrl', ['$scope', function ($scope) {
		this.selectedItem = null;
		this.searchText = null;
		this.querySearch = querySearch;
		this.tags = loadTags($scope.allTags);

		function querySearch(query) {
			var results = query ? this.tags.filter(createFilterFor(query)) : [];
			var newResults = [];
			results.forEach(function (val, index) {
				newResults.push(val.name);
			});
			return newResults;
		}

		function createFilterFor(query) {
			var lowercaseQuery;
			try {
				lowercaseQuery = query.toLowerCase();
			} catch (e) {
				lowercaseQuery = query;
			}
			return function filterFn(tag) {
				return (tag._lowername.indexOf(lowercaseQuery) === 0);
			};
		}

		function loadTags(tags) {
			return tags.map(function (tag) {
				try {
					tag._lowername = tag.name.toLowerCase();
				} catch (e) {
					tag._lowername = tag.name;
				}
				return tag;
			});
		}
	}]);
	app.controller('PostsCtrl', ['$scope', '$state', '$document', '$mdDialog', 'PostService', 'ToastService', 'classify', 'author', 'posts', function ($scope, $state, $document, $mdDialog, PostService, ToastService, classify, author, posts) {
		$scope.classify = classify.classify;
		$scope.author = author.users;
		$scope.posts = posts.posts;
		$scope.confirmPass = '';
		$scope.query = {
			author: '',
			classify: '',
			published: ''
		}
		$scope.editPost = function (id) {
			$state.go('main.editor', {
				id: id
			});
		}
		$scope.showRemoveDialog = function (post, ev, index) {
			if ($scope.global_user.power > post.author.power) {
				$scope.targetPost = post;
				$mdDialog.show({
					scope: $scope,
					templateUrl: '/backend/tpls/dialog_confirm_pass_post.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					preserveScope: true
				});
			} else {
				var confirm = $mdDialog.confirm()
					.title('你确定删除 ' + post.title + ' 这篇文章吗？')
					.content('<p>此操作没有备份！没有后悔药！</p>')
					.ariaLabel('removePostDialog')
					.targetEvent(ev)
					.ok('删除')
					.cancel('我冷静一下');

				var query = {
					id: post._id
				}
				$mdDialog.show(confirm).then(function () {
					PostService.removePost(query, function (res) {
						if (res.success) {
							ToastService.show($scope, {
								status: 'success',
								message: res.message,
								hideDelay: 3000
							});
							$mdDialog.cancel();
							$scope.posts.splice(index, 1);
						} else {
							ToastService.show($scope, {
								status: 'error',
								message: res.message,
								hideDelay: 6000,
								canClose: true
							});
						}
					});
				}, function () {
					ToastService.show($scope, {
						message: '取消了！'
					});
				});
			}
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.removePost = function () {
			var query = {
				id: $scope.targetPost._id,
				pass: $scope.confirmPass
			}
			PostService.removePost(query, function (res) {
				if (res.success) {
					ToastService.show($scope, {
						status: 'success',
						message: res.message,
						hideDelay: 3000
					});
					$mdDialog.cancel();
					$state.reload();
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						parent: $document[0].querySelector('#confirmPass'),
						canClose: true,
						position: 'top left'
					});
				}
			});
		}
		$scope.getPosts = function () {
			var query = {};
			for (var attr in $scope.query) {
				if ($scope.query[attr] || typeof $scope.query[attr] === 'boolean') {
					query[attr] = $scope.query[attr];
				}
			}
			PostService.getPosts(query)
				.then(function (result) {
					$scope.posts = result.posts;
				});
		}
	}]);
	app.controller('UsersCtrl', ['$scope', '$document', '$state', '$mdDialog', 'users', 'UserService', 'ToastService', function ($scope, $document, $state, $mdDialog, users, UserService, ToastService) {
		$scope.users = users.users;
		$scope.confirmPass = '';
		$scope.targetUser = {};
		$scope.goEditPage = function (id) {
			$state.go('main.user', {
				id: id
			});
		}
		$scope.showRemoveDialog = function (user, ev) {
			$scope.targetUser = user;
			$mdDialog.show({
				scope: $scope,
				templateUrl: '/backend/tpls/dialog_confirm_pass.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				preserveScope: true
			});
		}
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.removeUser = function () {
			UserService.removeUser($scope.targetUser._id, $scope.confirmPass, function (res) {
				if (res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'success',
						hideDelay: '3000'
					});
					$mdDialog.cancel();
					$state.reload();
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						parent: $document[0].querySelector('#confirmPass'),
						canClose: true,
						position: 'top left'
					});
				}
			});
		}
	}]);
	app.controller('UserCtrl', ['$scope', 'user', 'UserService', 'ToastService', '$localStorage', '$sessionStorage', function ($scope, user, UserService, ToastService, $localStorage, $sessionStorage) {
		$scope.user = user.user;
		if (!$scope.user.avatar) {
			if ($scope.user.sex) {
				$scope.user.avatar = '/backend/i/avatar_man.svg';
			} else {
				$scope.user.avatar = '/backend/i/avatar_woman.svg';
			}
		}
		if (typeof $scope.user.sex === 'boolean') {
			$scope.user.sex = $scope.user.sex.toString();
		}
		$scope.updateUser = function () {
			var updateData = {};

			function checkEmptyObj(Obj) {
				for (var attr in Obj) {
					return false;
				}
				return true;
			}
			for (var attr in $scope.user) {
				if ($scope.edit_user_form[attr]) {
					if ($scope.edit_user_form[attr].$dirty && $scope.edit_user_form[attr].$valid) {
						if ($scope.user[attr]) {
							updateData[attr] = $scope.user[attr];
						}
					}
				}
			}
			if (!checkEmptyObj(updateData)) {
				UserService.updateUser($scope.user._id, updateData, function (res) {
					if (res.success) {
						if ($scope.user._id.toString() === $scope.global_user._id.toString()) {
							var storage = $localStorage.user ? $localStorage : $sessionStorage;
							storage.user = {
								_id: $scope.user._id,
								name: $scope.user.name,
								power: $scope.user.power
							}
						}
						ToastService.show($scope, {
							message: res.message,
							status: 'success',
							hideDelay: '3000',
						});
					} else {
						ToastService.show($scope, {
							message: res.message,
							status: 'error',
							canClose: true
						});
					}
				});
			}
		}
	}]);
	app.controller('InviteCtrl', ['$scope', '$mdDialog', '$document', 'UserService', 'ToastService', '$localStorage', '$sessionStorage', function ($scope, $mdDialog, $document, UserService, ToastService, $localStorage, $sessionStorage) {
		$scope.invite = {
			email: '',
			power: 0
		}
		$scope.global_user = $localStorage.user || $sessionStorage.user;
		$scope.closeDialog = function () {
			$mdDialog.cancel();
		}
		$scope.inviteUser = function () {
			UserService.addUser($scope.invite, function (res) {
				if (!res.success) {
					ToastService.show($scope, {
						message: res.message,
						status: 'error',
						canClose: true,
						parent: $document[0].querySelector('#inviteUser'),
						position: 'top left'
					});
				} else {
					ToastService.show($scope, {
						message: res.message,
						status: 'success'
					})
					$mdDialog.cancel();
				}
			});
		}
	}]);

	app.controller('SettingsCtrl', ['$scope', '$http', '$localStorage', '$sessionStorage', 'SettingsService', 'ToastService', 'settings', function ($scope, $http, $localStorage, $sessionStorage, SettingsService, ToastService, settings) {
		var settings = settings.settings;
		$scope.token = $localStorage.token || $sessionStorage.token || $http.defaults.headers.common.accessToken;
		if (settings) {
			$scope.settings = {
				name: settings.name,
				show_num: settings.show_num,
				notice: settings.notice,
				summary: settings.summary,
				img_cover: settings.img_cover
			}
		} else {
			$scope.settings = {
				name: '',
				show_num: 5,
				notice: '',
				summary: '',
				img_cover: ''
			}
		}
		$scope.saveSettings = function () {
			SettingsService.saveSettings($scope.settings, function (result) {
				var status = result.success ? 'success' : 'error';
				var canClose = result.success ? false : true;
				ToastService.show($scope, {
					status: status,
					message: result.message,
					canClose: canClose,
					position: 'bottom right',
					hideDelay: 3000
				});
			});
		}
	}]);
	
}());
(function () {

	'use strict';
	
	var app = angular.module('app.directive', []);
	app.directive('compareTo', function () {
		return {
			require: 'ngModel',
			scope: {
				otherModelValue: '=compareTo'
			},
			link: function (scope, element, attributes, ngModel) {
				ngModel.$validators.compareTo = function (modelValue) {
					return modelValue === scope.otherModelValue;
				};
				scope.$watch('otherModelValue', function () {
					ngModel.$validate();
				});
			}
		};
	});
	app.directive('editor', ['$localStorage', '$sessionStorage', function ($localStorage, $sessionStorage) {
		return {
			restrict: 'E',
			replace: true,
			template: '<div id="editormd"></div>',
			link: function ($scope) {
				var token = $localStorage.token || $sessionStorage.token || '';
				var h = $(window).height() - $('#editor-header').outerHeight() - 2;
				var windowX = $(window).width();
				var showLineNum = windowX > 960 ? true : false;
				var editor = $scope.editor = editormd('editormd', {
					path: '/lib/editormd/lib/',
					height: h,
					markdown: $scope.post.md,
					watch: true,
					lineNumbers: showLineNum,
					htmlDecode: 'style,script,iframe',
					emoji: true,
					taskList: true,
					tocm: false, // Using [TOCM]
					tex: false, // 开启科学公式TeX语言支持，默认关闭
					flowChart: true, // 开启流程图支持，默认关闭
					sequenceDiagram: true, // 开启时序/序列图支持，默认关闭,
					imageUpload: true,
					imageFormats: ['jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'png', 'PNG', 'bmp', 'BMP', 'webp', 'WEBP'],
					imageUploadURL: '/duwei/x_upload/post/' + token,
					/*
					 上传的后台只需要返回一个JSON数据，结构如下：
					 {
					 success : 0 | 1,           // 0 表示上传失败，1 表示上传成功
					 message : "提示的信息，上传成功或上传失败及错误信息等。",
					 url     : "图片地址"        // 上传成功时才返回
					 }
					 */
					toolbarIcons: function () {
						if (windowX <= 600) {
							return ['undo', 'redo', '|', 'image', 'preview'];
						} else if (windowX > 600 && windowX <= 960) {
							return editormd.toolbarModes['simple'];
						} else {
							return editormd.toolbarModes['full'];
						}
					},
					onload: function () {
						if (windowX <= 600) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else if (windowX > 600 && windowX <= 960) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else {
							this.preview.show();
							this.codeMirror.css('width', this.editor.width() / 2);
						}
						var keyMap = {
							'Ctrl-S': $scope.savePost
						}
						if ($scope.post.author && Number($scope.global_user.power) === 0 && $scope.post.author._id.toString() !== $scope.global_user._id.toString()) {
							console.log('power limit!');
						} else {
							this.addKeyMap(keyMap);
						}
					},
					onresize: function () {
						var windowX = $(window).width();
						if (windowX <= 600) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else if (windowX > 600 && windowX <= 960) {
							this.preview.hide();
							this.codeMirror.css('width', this.editor.width());
						} else {
							this.preview.show();
							this.codeMirror.css('width', this.editor.width() / 2);
						}
					},
					onfullscreen: function () {
						$('#site-nav').hide();
					},
					onfullscreenExit: function () {
						$('#site-nav').show();
					},
					onpreviewing: function () {


					},
					onpreviewed: function () {

					}
				});
			}
		}
	}]);

}());
(function () {

	'use strict';
	
	var app = angular.module('app.filter', []);

	app.filter('publish', function () {
		return function (value) {
			if (value === true) {
				return '已发布';
			} else {
				return '未发布';
			}
		}
	});

	app.filter('power', function () {
		return function (value) {
			switch (value) {
			case 0:
				return '作者';
			case 1:
				return '管理员';
			case 2:
				return 'master';
			}
		}
	});

}());
(function () {

	'use strict';
	var app = angular.module('app.service',[]);
	app.factory('SignService', ['$http','$rootScope','authService','$state','$localStorage','$sessionStorage',function( $http,$rootScope,authService,$state,$localStorage,$sessionStorage){
		return {
			signin : function(user){
				$http.post('/duwei/x_signin',user)
				.success(function(data, status, headers, config){
					$http.defaults.headers.common.accessToken = data.token;
					authService.loginConfirmed(data, function(config) {  
						config.headers.accessToken = data.token;
						return config;
					});
				})
				.error(function(data, status, headers, config){
					$rootScope.$broadcast('event:auth-signin-failed', data);
				});
			},
			signout : function(){
				delete $http.defaults.headers.common.accessToken;
				$localStorage.$reset();
				$sessionStorage.$reset();	
				$state.go('signin',{},{ location: 'replace' });
			},
			signinCancelled : function(){
				authService.loginCancelled();
			},
			checkSignup: function(key){
				return $http.get('/duwei/x_signup',{
					params : key
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});	
			},
			signup: function(user,callback){
				$http.post('/duwei/x_signup',user)
				.success(callback)
				.error(callback);
			},
			checkReset: function(key){
				return $http.get('/duwei/x_reset',{
					params : key
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			applyReset:function(email,callback){
				$http.post('/duwei/x_applyreset',{
					email : email
				})
				.success(callback)
				.error(callback);
			},
			resetPass:function(data,callback){
				$http.post('/duwei/x_reset',data)
				.success(callback)
				.error(callback);
			}
		}
	}]);
	app.factory('ToastService', ['$mdToast', function($mdToast){
		return {
			show:function(scope,options){
				var toastOptions = {
					scope:scope,
					controller: 'ToastCtrl',
					templateUrl:'/backend/tpls/toast.html',
					parent:angular.element(document.body) ,
					preserveScope:true,
					hideDelay:6000,
					position:'top right'
				}
				if(options){
					for(var attr in toastOptions){
						if(options[attr]){
							toastOptions[attr] = options[attr];
						}
					}
					var message = options.message ? options.message : '';
					var status = options.status ? options.status : 'default';
					var canClose = options.canClose ? options.canClose : false;
					scope.toast = {
						message : message,
						status :  status,
						canClose: canClose
					}
					$mdToast.hide();
					if(message){
						$mdToast.show(toastOptions);
					}
				}
			},
			hide:function(){
				$mdToast.hide();
			}
		}
	}])
	app.factory('PostService', ['$http','authService', function($http){
		return {
			savePost : function(post,callback){
				$http.post('/duwei/x_posts',post)
				.success(callback)
				.error(callback);
			},
			removePost : function(query,callback){
				$http.delete('/duwei/x_posts',{
					params:query
				})
				.success(callback)
				.error(callback);		
			},
			updatePost : function(post,callback){
				$http.put('/duwei/x_posts/'+post.id,post)
				.success(callback)
				.error(callback);
			},
			getPosts : function(query){
				query = query || '';
				return $http.get('/duwei/x_posts',{
					params:query
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			getOnePost : function(id){
				if(id){
					return $http.get('/duwei/x_posts/'+id)
					.then(function(res){
						return res.data;
					},function(res){
						return res.data;
					});
				}else{
					return '';
				}
			},
			getClassify : function(){
				return $http.get('/duwei/x_classify')
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			}
		}
	}]);
	app.factory('UserService', ['$rootScope','$http',function($rootScope,$http){
		return {
			getUsers : function(query){
				return $http.get('/duwei/x_admin',{
					params:query
				})
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			getOneUser : function(id){
				if(id){
					return $http.get('/duwei/x_admin/'+id)
					.then(function(res){
						return res.data;
					},function(res){
						return res.data;
					});	
				}
			},
			addUser : function(data,callback){
				$http.post('/duwei/x_invite',data)
				.success(callback)
				.error(callback);
			},
			updateUser :function(id,data,callback){
				$http.put('/duwei/x_admin/'+id,data)
				.success(callback)
				.error(callback);
			},
			removeUser : function(id,pass,callback){
				$http.delete('/duwei/x_admin/'+id+'/'+pass)
				.success(callback)
				.error(callback);
			}
		}
	}]);
	app.factory('SettingsService', ['$http', function($http){
		return {
			getSettings : function(){
				return $http.get('/duwei/x_setting')
				.then(function(res){
					return res.data;
				},function(res){
					return res.data;
				});
			},
			saveSettings : function(data,callback){
				$http.post('/duwei/x_setting',data)
				.success(callback)
				.error(callback);
			}
		}
	}]);
}());