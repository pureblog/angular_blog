(function () {
	'use strict';
	var app = angular.module('app.service', []);
	app.factory('SettingsService', [
		'$http',
		function ($http) {
			return {
				getSettings: function () {
					return $http.get('/f_settings')
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				}
			};
		}
	]);
	app.factory('PostsService', [
		'$http',
		function ($http) {
			return {
				getClassify: function () {
					return $http.get('/f_classify')
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPosts: function (page) {
					return $http.get('/f_posts/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getRecentPosts : function () {
					var limit = 5;
					return $http.get('/f_posts/recentPost/' + limit)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostBySlug: function (slug) {
					return $http.get('/f_posts/slug/' + slug)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostsByClassify: function (classify, page) {
					return $http.get('/f_posts/classify/' + classify + '/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostsByTag: function (tag, page) {
					return $http.get('/f_posts/tag/' + tag + '/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostsByAuthor: function (author, page) {
					return $http.get('/f_posts/author/' + author + '/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				}
			};
		}
	]);
		
}());