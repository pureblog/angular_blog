(function () {
	'use strict';
	var app = angular.module('app.directive', []);	
	app.directive('articleRender', [
		function () {
			return {
				restrict: 'A',
				replace: false,
				link: function ($scope, iEle) {
					iEle[0].innerHTML= $scope.article.html;
				}
			}
		}
	]);
	
	app.directive('aboutRender', [
		function () {
			return {
				restrict: 'A',
				replace: false,
				link: function ($scope, iEle) {
					iEle[0].innerHTML= marked($scope.aboutText);
				}
			}
		}
	]);
}());