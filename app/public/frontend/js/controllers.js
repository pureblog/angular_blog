(function () {
	'use strict';
	var app = angular.module('app.controller', []);
	app.controller('HeaderCtrl', [
		'$scope',
		'$state',
		function ($scope, $state) {
			$scope.goHome = function () {
				return $state.go('index');
			};
			$scope.goAbout = function () {
				return $state.go('about');
			};
			
			$scope.currentState = $state.current.name;
			
		}
	]);
	app.controller('IndexCtrl', [
		'$scope',
		'$rootScope',
		'$stateParams',
		'data',
		'recentPosts',
		'classify',
		function ($scope, $rootScope, $stateParams, data, recentPosts, classify) {
			$scope.posts = data.posts;
			$scope.recentPosts = recentPosts.posts;
			$scope.total = data.total;
			$scope.classify = classify.classify;
			$scope.tags = classify.tags;

			$rootScope.settings = data.settings;
			$rootScope.site_description = 'web前端,NodeJS,AngularJS,Javascript原创博客';
		}
	]);
	app.controller('QueryCtrl', [
		'$scope',
		'$rootScope',
		'data',
		function ($scope, $rootScope, data) {
			$scope.posts = data.posts;
			$scope.total = data.total;

			$rootScope.settings = data.settings;
			$rootScope.site_description = 'web前端,NodeJS,AngularJS,Javascript原创博客';
			
		}
	]);
	app.controller('ArticleCtrl', [
		'$scope',
		'$rootScope',
		'article',
		'recentPosts',
		'classify',
		'settings',
		function ($scope, $rootScope, article, recentPosts, classify, settings) {

			$scope.article = article.post;
			$scope.recentPosts = recentPosts.posts;
			$scope.classify = classify.classify;
			$scope.tags = classify.tags;

			$rootScope.settings = settings.settings;
			$rootScope.site_description = $scope.article.summary;
		}
	]);
	app.controller('PageCtrl', [
		'$scope',
		'$rootScope',
		'$stateParams',
		'$state',
		'$location',
		function ($scope, $rootScope, $stateParams, $state, $location) {
			$scope.pageTotal = Math.ceil($scope.total / $rootScope.settings.show_num);//总页数
			$scope.pageNow = Number($stateParams.page) || 1; //当前页
			function getPrePageUrl() {
				var url = $location.url().split('page');
				if ($scope.pageNow === 2) {
					return url[0];
				} else if ($scope.pageNow === 1) {
					return '';
				} else {
					return url[0] + 'page/' + ($scope.pageNow - 1) + '/';
				}
			}
			function getNextPageUrl() {
				var url = $location.url().split('page');
				if ($scope.pageNow === $scope.pageTotal) {
					return '';
				} else {
					return url[0] + 'page/' + ($scope.pageNow + 1) + '/';
				}
			}
			$scope.preUrl = getPrePageUrl();
			$scope.nextUrl = getNextPageUrl();
		}
	]);
	app.controller('AboutCtrl', [
		'$scope',
		'$rootScope',
		'settings',
		'data',
		function ($scope, $rootScope, settings, data) {
			$rootScope.settings = settings.settings;
			$scope.aboutText = data.data;
		}
	]);
	app.controller('ErrorCtrl', [
		'$scope',
		'$state',
		function ($scope, $state) {
			$scope.goHome = function () {
				return $state.go('index', {}, {location: 'replace'});
			};
		}
	]);
}());