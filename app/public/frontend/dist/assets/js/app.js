(function () {
	'use strict';
	var app = angular.module('app', [
		'ui.router',
		'oc.lazyLoad',
		'app.controller',
		'app.service',
		'app.directive'
	]);
	app.config([
		'$locationProvider',
		'$stateProvider',
		'$urlRouterProvider',
		function ($locationProvider, $stateProvider, $urlRouterProvider) {
			$locationProvider.html5Mode({
				enabled: true,
				requireBase: false
			});
			$locationProvider.hashPrefix('!');
			$stateProvider
				.state('index', {
					url: '/',
					controller: 'IndexCtrl',
					templateUrl: '/frontend/pages/index.html',
					resolve: {
						'data': [
							'PostsService',
							function (PostsService) {
								var page = 1;
								return PostsService.getPosts(page);
							}
						],
						'recentPosts': [
							'PostsService',
							function (PostsService) {
								return PostsService.getRecentPosts();
							}
						],
						'classify': [
							'PostsService',
							function (PostsService) {
								return PostsService.getClassify();
							}
						]
					}
				})
				.state('page', {
					url: '/page/:page/',
					controller: 'IndexCtrl',
					templateUrl: '/frontend/pages/page.html',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var page = $stateParams.page;
								return PostsService.getPosts(page);
							}
						],
						'recentPosts': [
							'PostsService',
							function (PostsService) {
								return PostsService.getRecentPosts();
							}
						],
						'classify': [
							'PostsService',
							function (PostsService) {
								return PostsService.getClassify();
							}
						]
					}
				})
				.state('classify', {
					url: '/classify/:classify/',
					templateUrl: '/frontend/pages/classify.html',
					controller: 'QueryCtrl',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var classify = $stateParams.classify,
									page = 1;
								return PostsService.getPostsByClassify(classify, page);
							}
						]
					}
				})
				.state('classify_page', {
					url: '/classify/:classify/page/:page/',
					templateUrl: '/frontend/pages/classify.html',
					controller: 'QueryCtrl',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var classify = $stateParams.classify,
									page = $stateParams.page;
								return PostsService.getPostsByClassify(classify, page);
							}
						]
					}
				})
				.state('tag', {
					url: '/tag/:tag/',
					templateUrl: '/frontend/pages/classify.html',
					controller: 'QueryCtrl',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var tag = $stateParams.tag,
									page = 1;
								return PostsService.getPostsByTag(tag, page);
							}
						]
					}
				})
				.state('tag_page', {
					url: '/tag/:tag/page/:page/',
					templateUrl: '/frontend/pages/classify.html',
					controller: 'QueryCtrl',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var tag = $stateParams.tag,
									page = $stateParams.page;
								return PostsService.getPostsByTag(tag, page);
							}
						]
					}
				})
				.state('author', {
					url: '/author/:author/',
					templateUrl: '/frontend/pages/author.html',
					controller: 'QueryCtrl',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var author = $stateParams.author,
									page = 1;
								return PostsService.getPostsByAuthor(author, page);
							}
						]
					}
				})
				.state('author_page', {
					url: '/author/:author/page/:page/',
					templateUrl: '/frontend/pages/author.html',
					controller: 'QueryCtrl',
					resolve: {
						'data': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var author = $stateParams.author,
									page = $stateParams.page;
								return PostsService.getPostsByAuthor(author, page);
							}
						]
					}
				})
				.state('article', {
					url: '/article/:slug/',
					controller: 'ArticleCtrl',
					templateUrl: '/frontend/pages/article.html',
					resolve: {
						'article': [
							'PostsService',
							'$stateParams',
							function (PostsService, $stateParams) {
								var slug = $stateParams.slug;
								return PostsService.getPostBySlug(slug);
							}
						],
						'recentPosts': [
							'PostsService',
							function (PostsService) {
								return PostsService.getRecentPosts();
							}
						],
						'classify': [
							'PostsService',
							function (PostsService) {
								return PostsService.getClassify();
							}
						],
						'settings': [
							'SettingsService',
							function (SettingsService) {
								return SettingsService.getSettings();
							}
						]
					}
				})
				.state('about', {
					url: '/about/',
					controller: 'AboutCtrl',
					templateUrl: '/frontend/pages/about.html',
					resolve: {
						loadjs: [
							'$ocLazyLoad',
							'$rootScope',
							function ($ocLazyLoad, $rootScope) {
								$rootScope.ocLazyLoad_loading = true;
								return $ocLazyLoad.load('//cdn.bootcss.com/marked/0.3.5/marked.min.js')
									.then(function () {
										$rootScope.ocLazyLoad_loading = false;
									});
							}
						],
						'data': [
							'$http',
							function ($http) {
								return $http.get('/frontend/pages/about.md');
							}
						],
						'settings': [
							'SettingsService',
							function (SettingsService) {
								return SettingsService.getSettings();
							}
						]
					}
				})
				.state('404', {
					url: '/error/404/',
					controller: 'ErrorCtrl',
					templateUrl: '/frontend/pages/404.html'
				})
				.state('500', {
					url: '/error/500/',
					controller: 'ErrorCtrl',
					templateUrl: '/frontend/pages/500/html'
				});
			$urlRouterProvider
				.when('', '/')
				.otherwise('/error/404/');
		}
	]);
	app.run([
		'$state',
		'$rootScope',
		'$templateCache',
		'$http',
		'$location',
		function ($state, $rootScope, $templateCache, $http, $location) {
			var url = $location.url(),
				tpls = [
					'/frontend/pages/index.html',
					'/frontend/pages/page.html',
					'/frontend/pages/query.html',
					'/frontend/pages/classify.html',
					'/frontend/pages/author.html',
					'/frontend/pages/article.html',
					'/frontend/tpls/footer.html',
					'/frontend/tpls/header.html',
					'/frontend/tpls/posts.html',
					'/frontend/tpls/sidebar.html'
				];
			if (url.charAt(url.length - 1) !== '/') {
				return $location.path(url + '/').replace();
			}
			$rootScope.$on('event:not-found', function () {
				return $state.go('404', {}, {location: 'replace'});
			});
			$rootScope.$on('event:server-error', function () {
				return $state.go('500', {}, {location: 'replace'});
			});
			angular.forEach(tpls, function (tpl) {
				$http.get(tpl, {cache: $templateCache});
			});
		}
	]);
	//loading 
	app.config([
		'$httpProvider',
		function ($httpProvider) {
			var numLoadings = 0;
			$httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
			$httpProvider.interceptors.push([
				'$q',
				'$rootScope',
				function ($q, $rootScope) {
					return {
						'request': function (config) {
							numLoadings++;
							$rootScope.global_loading = true;
							return config;
						},
						'response': function (response) {
							if (!(--numLoadings)) {
								$rootScope.global_loading = false;
							}
							return response;
						},
						'responseError': function (rejection) {
							if (rejection.status === 404) {
								$rootScope.$broadcast('event:not-found');
							} else if (rejection.status >= 500) {
								$rootScope.$broadcast('event:server-error');
							}
							numLoadings--;
							$rootScope.global_loading = false;
							return $q.reject(rejection);
						}
					};
				}
			]);
		}
	]);
}());
(function () {
	'use strict';
	var app = angular.module('app.controller', []);
	app.controller('HeaderCtrl', [
		'$scope',
		'$state',
		function ($scope, $state) {
			$scope.goHome = function () {
				return $state.go('index');
			};
			$scope.goAbout = function () {
				return $state.go('about');
			};
			
			$scope.currentState = $state.current.name;
			
		}
	]);
	app.controller('IndexCtrl', [
		'$scope',
		'$rootScope',
		'$stateParams',
		'data',
		'recentPosts',
		'classify',
		function ($scope, $rootScope, $stateParams, data, recentPosts, classify) {
			$scope.posts = data.posts;
			$scope.recentPosts = recentPosts.posts;
			$scope.total = data.total;
			$scope.classify = classify.classify;
			$scope.tags = classify.tags;

			$rootScope.settings = data.settings;
			$rootScope.site_description = 'web前端,NodeJS,AngularJS,Javascript原创博客';
		}
	]);
	app.controller('QueryCtrl', [
		'$scope',
		'$rootScope',
		'data',
		function ($scope, $rootScope, data) {
			$scope.posts = data.posts;
			$scope.total = data.total;

			$rootScope.settings = data.settings;
			$rootScope.site_description = 'web前端,NodeJS,AngularJS,Javascript原创博客';
			
		}
	]);
	app.controller('ArticleCtrl', [
		'$scope',
		'$rootScope',
		'article',
		'recentPosts',
		'classify',
		'settings',
		function ($scope, $rootScope, article, recentPosts, classify, settings) {

			$scope.article = article.post;
			$scope.recentPosts = recentPosts.posts;
			$scope.classify = classify.classify;
			$scope.tags = classify.tags;

			$rootScope.settings = settings.settings;
			$rootScope.site_description = $scope.article.summary;
		}
	]);
	app.controller('PageCtrl', [
		'$scope',
		'$rootScope',
		'$stateParams',
		'$state',
		'$location',
		function ($scope, $rootScope, $stateParams, $state, $location) {
			$scope.pageTotal = Math.ceil($scope.total / $rootScope.settings.show_num);//总页数
			$scope.pageNow = Number($stateParams.page) || 1; //当前页
			function getPrePageUrl() {
				var url = $location.url().split('page');
				if ($scope.pageNow === 2) {
					return url[0];
				} else if ($scope.pageNow === 1) {
					return '';
				} else {
					return url[0] + 'page/' + ($scope.pageNow - 1) + '/';
				}
			}
			function getNextPageUrl() {
				var url = $location.url().split('page');
				if ($scope.pageNow === $scope.pageTotal) {
					return '';
				} else {
					return url[0] + 'page/' + ($scope.pageNow + 1) + '/';
				}
			}
			$scope.preUrl = getPrePageUrl();
			$scope.nextUrl = getNextPageUrl();
		}
	]);
	app.controller('AboutCtrl', [
		'$scope',
		'$rootScope',
		'settings',
		'data',
		function ($scope, $rootScope, settings, data) {
			$rootScope.settings = settings.settings;
			$scope.aboutText = data.data;
		}
	]);
	app.controller('ErrorCtrl', [
		'$scope',
		'$state',
		function ($scope, $state) {
			$scope.goHome = function () {
				return $state.go('index', {}, {location: 'replace'});
			};
		}
	]);
}());
(function () {
	'use strict';
	var app = angular.module('app.directive', []);	
	app.directive('articleRender', [
		function () {
			return {
				restrict: 'A',
				replace: false,
				link: function ($scope, iEle) {
					iEle[0].innerHTML= $scope.article.html;
				}
			}
		}
	]);
	
	app.directive('aboutRender', [
		function () {
			return {
				restrict: 'A',
				replace: false,
				link: function ($scope, iEle) {
					iEle[0].innerHTML= marked($scope.aboutText);
				}
			}
		}
	]);
}());
(function () {
	'use strict';
	var app = angular.module('app.service', []);
	app.factory('SettingsService', [
		'$http',
		function ($http) {
			return {
				getSettings: function () {
					return $http.get('/f_settings')
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				}
			};
		}
	]);
	app.factory('PostsService', [
		'$http',
		function ($http) {
			return {
				getClassify: function () {
					return $http.get('/f_classify')
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPosts: function (page) {
					return $http.get('/f_posts/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getRecentPosts : function () {
					var limit = 5;
					return $http.get('/f_posts/recentPost/' + limit)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostBySlug: function (slug) {
					return $http.get('/f_posts/slug/' + slug)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostsByClassify: function (classify, page) {
					return $http.get('/f_posts/classify/' + classify + '/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostsByTag: function (tag, page) {
					return $http.get('/f_posts/tag/' + tag + '/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				},
				getPostsByAuthor: function (author, page) {
					return $http.get('/f_posts/author/' + author + '/' + page)
						.then(function (res) {
							return res.data;
						}, function (res) {
							return res.data;
						});
				}
			};
		}
	]);
		
}());