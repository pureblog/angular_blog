(function () {
	
	'use strict';
	
	var validator = require('validator'),
		_ = require('lodash'),
		utils = require('../proxy/utils');

	exports.admin = function (req, res, next) {
		var allFields = [
			'_id',
			'name',
			'email',
			'pass',
			'password',
			'rePass',
			'rePassword',
			'posts',
			'power',
			'sex',
			'qq',
			'phone',
			'bio',
			'active',
			'locked',
			'create_at',
			'update_at'
		];
		var incomingData = _.isEmpty(req.query) ? req.body : req.query,
			data = _.pick(incomingData, allFields),
			re;
		for (var attr in data) {
			if (attr === 'sex' || attr === 'active' || attr === 'locked') {
				var val = utils.turnBoolean(data[attr]);
				if (!_.isBoolean(val)) {
					res.status(422);
					return res.json({
						success: 0,
						message: attr + '字段必须是布尔值！'
					});
				} else {
					data[attr] = val;
				}
			} else if (attr === 'pass' || attr === 'password') {
				var pass = data.pass || data.password;
				if (!validator.isLength(pass, 6, 16)) {
					res.status(422);
					return res.json({
						success: 0,
						message: '密码格式不对!'
					});
				} else {
					data.password = data.pass = pass;
					if (_.has(data, 'rePass') || _.has(data, 'rePassword')) {
						var rePass = data.rePass || data.rePassword;
						if (!validator.isLength(rePass, 6, 16)) {
							res.status(422);
							return res.json({
								success: 0,
								message: '密码格式不对!'
							});
						} else {
							if (rePass !== pass) {
								res.status(422);
								return res.json({
									success: 0,
									message: '两次密码不一致！'
								});
							}
						}
					}
				}
			} else {
				switch (attr) {
				case 'name':
					var name = validator.trim(data.name),
						re2 = /^[a-zA-Z]{5,16}$/;//全英文
						re = /^[\u4e00-\u9fa5]{2,4}$/;//全中文
						
					if ((!re.test(name)) && (!re2.test(name))) {
						res.status(422);
						return res.json({
							success: 0,
							message: '姓名格式不对！'
						});
					}
					data.name = name;
					break;
				case 'email':
					var email = validator.trim(data.email).toLowerCase();
					if (!validator.isEmail(email)) {
						res.status(422);
						return res.json({
							success: 0,
							message: '邮箱格式不对!'
						});
					}
					data.email = email;
					break;
				case 'power':
					var power = Number(validator.trim(data.power));
					if (_.isNaN(power)) {
						res.status(422);
						return res.json({
							success: 0,
							message: '权限格式不对!'
						});
					}
					data.power = power;
					break;
				case 'qq':
					re = /^[1-9][0-9]{4,9}$/;
					var qq = validator.trim(data.qq);
					if (!re.test(qq)) {
						res.status(422);
						return res.json({
							success: 0,
							message: '请输入正确的QQ号码!'
						});
					}
					data.qq = qq;
					break;
				case 'phone':
					re = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/;
					var phone = validator.trim(data.phone);
					if (!re.test(phone)) {
						res.status(422);
						return res.json({
							success: 0,
							message: '请输入正确的手机号码!'
						});
					}
					data.phone = phone;
					break;
				case 'bio':
					var bio = validator.trim(data.bio).toString().substr(0, 100);
					data.bio = bio;
					break;
				}
			}
		}
		req.data = data;
		next();
	};

	exports.posts = function (req, res, next) {
		var allFields = [
			'_id',
			'title',
			'slug',
			'author',
			'classify',
			'tags',
			'md',
			'html',
			'summary',
			'cover',
			'hits',
			'published',
			'create_at',
			'publish_at',
			'update_at'
		];
		var incomingData = _.isEmpty(req.query) ? req.body : req.query;
		var data = _.pick(incomingData, allFields);
		if (_.has(data, 'published')) {
			var val = utils.turnBoolean(data.published);
			if (!_.isBoolean(val)) {
				return res.json({
					success: 0,
					message: 'published字段必须是布尔值！'
				});
			} else {
				data.published = val;
			}
		}
		if (_.has(data, 'hits')) {
			var hits = Number(validator.trim(data.hits));
			if (_.isNaN(hits)) {
				res.status(422);
				return res.json({
					success: 0,
					message: 'hits格式不对!'
				});
			}
			data.hits = hits;
		}
		if (_.has(data, 'summary')) {
			var summary = validator.trim(data.summary).toString().substr(0, 150);
			data.summary = summary;
		}
		if (_.has(data, 'tags')) {
			if (_.isArray(data.tags)) {
				data.tags.length = data.tags.length > 5 ? 5 : data.tags.length; //强制保留5个tag
			}
		}
		req.data = data;
		next();
	};

	exports.settings = function (req, res, next) {
		var allFields = [
			'name',
			'summary',
			'show_num',
			'notice',
			'img_cover'
		];
		var incomingData = _.isEmpty(req.query) ? req.body : req.query;
		var data = _.pick(incomingData, allFields);
		if (_.has(data, 'show_num')) {
			var show_num = Number(validator.trim(data.show_num));
			if (_.isNaN(show_num)) {
				res.status(422);
				return res.json({
					success: 0,
					message: 'show_num必须是数字!'
				});
			}
			data.show_num = show_num;
		}
		req.data = data;
		next();
	};

}());
