(function () {

	'use strict';
	
	var jwt = require('jsonwebtoken'),
		validator = require('validator'),
		utility = require('utility'),
		async = require('async'),
		config = require('../config'),
		Admin = require('../models').Admin;

	function decodeToken(res, token, cb) {
		jwt.verify(token, config.token_key, function (err, decoded) {
			if (err) {
				return res.status(401).json({
					success: 0,
					message: '非法token！'
				});
			} else {
				return cb(decoded);
			}
		});
	}

	exports.token = function (req, res, next) {
		var token = req.headers.accesstoken || req.body.token || req.query.token || req.params.token;
		if (!token) {
			return res.status(401).json({
				success: 0,
				message: '请提供token!'
			});
		} else {
			decodeToken(res, token, function (decoded) {
				req.User = decoded;
				next();
			});
		}
	};

	exports.masterToken = function (req, res, next) {
		var token = req.headers.accesstoken  || req.body.token || req.query.token;
		if (!token) {
			return res.status(401).json({
				success: 0,
				message: '请提供token!'
			});
		} else {
			decodeToken(res, token, function (decoded) {
				if (decoded.power !== 2) {
					return res.status(403).json({
						success: 0,
						message: '你没有权限!'
					});
				} else {
					req.User = decoded;
					next();
				}
			});
		}
	};

	exports.adminToken = function (req, res, next) {
		var token = req.headers.accesstoken  || req.body.token || req.query.token;
		if (!token) {
			return res.status(401).json({
				success: 0,
				message: '请提供token!'
			});
		} else {
			decodeToken(res, token, function (decoded) {
				if (decoded.power < 1) {
					return res.status(403).json({
						success: 0,
						message: '你没有权限!'
					});
				} else {
					req.User = decoded;
					next();
				}
			});
		}

	};

	exports.resetKey = function (req, res, next) {
		function getKey(cb) {
			var key = validator.trim(req.body.key) || validator.trim(req.query.key) || '';
			if (!key) {
				return cb('无效的重置链接');
			}
			cb(null, key);
		}

		function checkKey(key, cb) {
			Admin.findOne({'retrieve_key': key}, function (err, user) {
				if (!user) {
					return cb('无效的重置链接');
				}
				cb(null, user);
			});
		}

		function checkTime(user, cb) {
			var oneday = 24 * 60 * 60 * 1000,
				now = new Date().getTime();
			if (now - user.retrieve_time > oneday) {
				user.retrieve_key = null;
				user.retrieve_time = null;
				user.save(function (err) {
					if (err) {
						return next(err);
					}
					return cb('链接已经过期,请重新申请!');
				});
			} else {
				cb(null, user);
			}
		}

		async.waterfall([getKey, checkKey, checkTime], function (err, user) {
			if (err) {
				return res.status(403).json({
					success: 0,
					message: err
				});
			} else {
				req.User = user;
				next();
			}
		});
	};

	exports.signup = function (req, res, next) {
		function checkMaster(cb) {
			var key = validator.trim(req.body.key) || validator.trim(req.query.key) || '';
			if (!key) {
				Admin.find({}, function (err, users) {
					if (users && users.length) {
						return cb('拒绝注册!');
					} else { //站长注册
						req.User = {};
						req.User.power = 2;
						next();
					}
				});
			} else {
				cb(null, key);
			}
		}

		function checkKey(key, cb) {
			Admin.findOne({'retrieve_key': key}, function (err, user) {
				if (!user) {
					return cb('错误的注册链接!');
				}
				cb(null, user);
			});
		}

		function checkTime(user, cb) {
			var now = new Date().getTime(),
				oneDay = 1000 * 60 * 60 * 24;
			if (!user.retrieve_time || now - user.retrieve_time > 7 * oneDay) {
				Admin.remove({'_id': user._id}, function (err) {
					if (err) {
						return next(err);
					}
					return cb('链接已经过期!');
				});
			}
			cb(null, user);
		}

		async.waterfall([checkMaster, checkKey, checkTime], function (err, user) {
			if (err) {
				return res.status(403).json({
					success: 0,
					message: err
				});
			} else {
				req.User = user;
				next();
			}
		});
	};
	
	exports.confirmPass = function (req, res, next) {
		var pass = req.params.pass || req.params.password || '';
		if (!pass) {
			return res.json({
				success: 0,
				message: '请输入密码！'
			});
		} else {
			pass =  utility.sha1(pass);
			Admin.findById(req.User._id, function (err, user) { //token里面的密码可能和数据库不统一
				if (pass !== user.password) {
					return res.status(422).json({
						success: 0,
						message: '密码不正确!'
					});
				} else {
					next();
				}
			});
		}
	};
	
}());