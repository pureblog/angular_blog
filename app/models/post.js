(function () {
	
	'use strict';
	
	var mongoose  = require('mongoose'),
		Schema    = mongoose.Schema,
		ObjectId  = Schema.ObjectId,

		PostSchema = new Schema({
			title: {type: String},
			slug: {type: String},
			author: {type: ObjectId, ref: 'Admin'},
			classify: {
				name : {type: String },
				slug : {type: String }
			},
			tags: [{
				name: {type: String },
				slug: {type: String }
			}],
			md: {type: String},
			html: {type: String},
			summary:  {type: String },
			cover: {type: String },
			comments: [{
				create_at: {type: Date, default: Date.now},
				author_id: {type: ObjectId },
				body: {type: String }
			}],
			hits: {type: Number, default: 0},
			published: {type: Boolean, default: false},
			create_at: {type: Date, default: Date.now},
			publish_at: {type: Date },
			update_at: {type: Date, default: Date.now}
		});

	PostSchema.index({slug: 1}, {unique: true});

	mongoose.model('Post', PostSchema);
	
}());