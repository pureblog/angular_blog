(function () {

	'use strict';
	
	var mongoose  = require('mongoose'),
		Schema    = mongoose.Schema,

		SettingSchema = new Schema({
			name: {type: String},
			summary: {type: String},
			show_num: {type: Number},
			notice: {type: String },
			img_cover: {type: String}
		});
	
	mongoose.model('Setting', SettingSchema);
	
}());