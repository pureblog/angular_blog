(function () {
	
	'use strict';
	
	var mongoose  = require('mongoose'),
		Schema    = mongoose.Schema,

		AdminSchema = new Schema({
			name: { type: String},
			email: { type: String},
			password: { type: String },
			power: {type: Number, defaut: 0},
			sex: {type: Boolean},  //true:男
			qq : { type: Number},
			phone: {type: Number},
			slug: {type: String },
			bio: {type: String},
			avatar: {type: String},
			create_at: {type: Date, default: Date.now},
			update_at: {type: Date, default: Date.now},
			retrieve_time: {type: Number},
			retrieve_key: {type: String},
			active: {type: Boolean, default: true},
			locked: {type: Boolean, default: false}
		});

	AdminSchema.index({email: 1}, {unique: true});

	mongoose.model('Admin', AdminSchema);
	
}());