(function () {
	
	'use strict';
	
	var mongoose = require('mongoose'),
		config = require('../config');

	mongoose.connect(config.db + config.db_name, function (err) {
		if (err) {
			console.error('connect to %s error: ', config.db, err.message);
			process.exit(1);
		}
	});

	require('./admin');
	require('./post');
	require('./setting');

	exports.Admin = mongoose.model('Admin');
	exports.Post = mongoose.model('Post');
	exports.Setting = mongoose.model('Setting');
	
}());