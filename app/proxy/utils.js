(function () {

	//'use strict';
	
	var fs = require('fs'),
		path = require('path'),
		cmdMode;

	exports.turnBoolean = function (val) {
		var newVal;
		if (typeof val !== 'boolean') {
			try {
				newVal = eval(val);
				return newVal;
			} catch (e) {
				return '字段必须是布尔值！';
			}
		} else {
			return val;
		}
	};

	var makeDirSync = exports.makeDirSync = function (dirname, mode) {
		cmdMode = mode || 0777;
		if (fs.existsSync(dirname)) {
			return true;
		} else {
			if (makeDirSync(path.dirname(dirname), mode)) {
				fs.mkdirSync(dirname, mode);
				return true;
			}
		}
	};

	var makeDir = exports.makeDir = function (dirpath, callback, mode) {
		cmdMode = mode || 0777;
		fs.exists(dirpath, function (exists) {
			if (exists) {
				callback();
			} else {
				makeDir(path.dirname(dirpath), mode, function () {
					fs.mkdir(dirpath, mode, callback);
				});
			}
		});
	};
}());