(function () {

	'use strict';
	
	var nodemailer = require('nodemailer');
	var config = require('../config');
	var transport = nodemailer.createTransport({
		service: config.mail.service,
		auth: {
			user: config.mail.auth.user,
			pass: config.mail.auth.pass
		}
	});

	function sendMail(mailOptions) {
		transport.sendMail(mailOptions, function (err) {
			if (err) console.log(err);
		});
	}

	exports.sendResetPassMail = function (towho, name, key) {
		var mailOptions = {
			from: config.mail.auth.user,
			to: towho,
			subject: config.name + ' 管理员密码找回 (系统邮件，请勿回复)',
			html: '<p>您好：' + name + '</p>' +
				'<p>我们收到您在' + config.name + '重置密码的请求，请在24小时内单击下面的链接来重置密码：</p>' +
				'<a href="' + config.host + config.backendUrl + '/resetpass?key=' + key + '">重置密码链接</a>' +
				'<p>若您没有在' + config.name + '填写过找回密码信息，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
				'<p>' + config.name + '谨上。</p>'
		}
		sendMail(mailOptions);
	}

	exports.sendInviteMail = function (towho, name, key) {
		var mailOptions = {
			from: config.mail.auth.user,
			to: towho,
			subject: name + ' 邀请你成为' + config.name + '管理员 (系统邮件，请勿回复)',
			html: '<p>你好!</p>' +
				'<p>你被' + name + '邀请成为' + config.name + '管理员,请在7天内单击下面的链接来激活账户：</p>' +
				'<a href="' + config.host + config.backendUrl + '/signup?key=' + key + '">点击填写必要信息激活您的账号</a>' +
				'<p>若您没有申请' + config.name + '管理员，说明有人滥用了您的电子邮箱，请删除此邮件，我们对给您造成的打扰感到抱歉。</p>' +
				'<p>' + config.name + '谨上。</p>'
		}
		sendMail(mailOptions);
	}

	exports.sendUpdateUserMail = function (towho, name, email) {
		var mailOptions = {
			from: config.mail.auth.user,
			to: towho,
			subject: name + ' 修改了你的个人资料 (系统邮件，请勿回复)',
			html: '<p>你好!</p>' +
				'<p>你被' + name + '修改了个人资料：</p>' +
				'<p>详情请联系:' + email + '</p>' +
				'<p>' + config.name + '谨上。</p>'
		}
		sendMail(mailOptions);
	}

	exports.sendBugReportMail = function (from, name, subject, html) {
		var mailOptions = {
			from: from,
			to: config.getMail,
			subject: subject + '(由' + name + '反馈)',
			html: html
		}
		sendMail(mailOptions);
	}

}());