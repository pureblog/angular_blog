(function () {
	
	'use strict';
	
	var express = require('express'),
		router = express.Router(),
		path = require('path'),
		api = require('../controllers/frontend');

	//settings
	router.get('/f_settings',
		api.getSettings
		);
	
	//classify&tag
	router.get('/f_classify',
		api.getClassify
		);
	
	//posts
	router.get('/f_posts/:page',
		api.getPostsLimit
		);
	router.get('/f_posts/recentPost/:limit',
		api.getRecentPost
		);
	router.get('/f_posts/slug/:slug',
		api.getOnePostBySlug
		);
	router.get('/f_posts/classify/:classify/:page',
		api.getPostsLimitByClassify
		);
	router.get('/f_posts/tag/:tag/:page',
		api.getPostsLimitByTag
		);
	router.get('/f_posts/author/:author/:page',
		api.getPostsLimitByAuthor
		);
	//for angular
	router.get('*', function (req, res) {
		res.sendFile(path.join(__dirname, '../public/frontend/index.html'));
	});

	module.exports = router;
	
}());