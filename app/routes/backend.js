(function () {
	
	'use strict';
	
	var express = require('express'),
		router = express.Router(),
		path = require('path'),

	//middlewares
		validator = require('../middlewares/validator'),
		access = require('../middlewares/access'),

	//controllers
		signController = require('../controllers/sign'),
		adminController = require('../controllers/admin'),
		settingController = require('../controllers/setting'),
		postController = require('../controllers/post'),
		uploadController = require('../controllers/upload'),
		reportController = require('../controllers/report');
		
	//bug
	router.post('/x_report',
		access.token,
		reportController.bug
		);
	//upload
	router.post('/x_upload/bug',
		access.token,
		uploadController.reportBug
		);
	router.post('/x_upload/post/:token',
		access.token,
		uploadController.post
		);
	router.post('/x_upload/postCover',
		access.token,
		uploadController.postCover
		);
	router.post('/x_upload/avatar/:id',
		access.token,
		uploadController.avatar
		);
	router.post('/x_upload/cover',
		access.masterToken,
		uploadController.cover
		);

	//sign
	router.post('/x_signin',
		validator.admin,
		signController.signin
		);
	router.get('/x_signup',
		access.signup,
		signController.showSignup
		);
	router.post('/x_signup',
		access.signup,
		validator.admin,
		signController.signup
		);
	router.post('/x_invite',
		access.adminToken,
		validator.admin,
		signController.inviteUser
		);
	router.get('/x_reset',
		access.resetKey,
		signController.showReset
		);
	router.post('/x_applyreset',
		validator.admin,
		signController.applyReset
		);
	router.post('/x_reset',
		access.resetKey,
		validator.admin,
		signController.resetPass
		);

	//admin
	router.get('/x_admin',
		access.token,
		validator.admin,
		adminController.getAdmins
		);
	router.get('/x_admin/:id',
		access.token,
		adminController.getOneAdmin
		);
	router.put('/x_admin/:id',
		access.token,
		validator.admin,
		adminController.updateUser
		);
	router.delete('/x_admin/:id/:pass',
		access.masterToken,
		access.confirmPass, //确认password
		adminController.removeUser
		);

	//post
	router.get('/x_classify',
		access.token,
		postController.getClassify
		);
	router.get('/x_posts',
		access.token,
		validator.posts,
		postController.getPosts
		);
	router.get('/x_posts/:id',
		access.token,
		postController.getOnePost
		);
	router.post('/x_posts',
		access.token,
		validator.posts,
		postController.newPost
		);
	router.put('/x_posts/:id',
		access.token,
		validator.posts,
		postController.updatePost
		);
	router.delete('/x_posts',
		access.token,
		postController.removePost
		);

	//setting
	router.get('/x_setting',
		access.token,
		settingController.getSettings
		);
	router.post('/x_setting',
		access.masterToken,
		validator.settings,
		settingController.saveSettings
		);

	//for angular
	router.get('*', function (req, res) {
		res.sendFile(path.join(__dirname, '../public/backend/index.html'));
	});

	module.exports = router;
}());