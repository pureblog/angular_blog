(function () {
	
	'use strict';
	
	var mail = require('../proxy/mail');
	var config = require('../config');
	exports.bug = function (req, res) {
		var subject = req.body.subject;
		var detail = req.body.detail;
		var img = req.body.img;
		var from = req.User.email;
		var name = req.User.name;
		var html;
		var imgHTML = '';
		if (img) {
			img.forEach(function (i) {
				imgHTML += '<p><img src="' + config.host + i + '" /></p>';
			});
		}
		html = '<p>' + detail + '</p>' + imgHTML;
		mail.sendBugReportMail(from, name, subject, html);
		res.json({
			success: 1,
			message: '感谢你的反馈!'
		});
	}

}());