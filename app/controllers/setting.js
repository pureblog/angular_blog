(function (){

	'use strict';
	
	var Setting = require('../models').Setting;

	exports.getSettings = function (req, res, next) {
		Setting.findOne({}, function (err, doc) {
			if (err) return next(err);
			res.json({
				success: 1,
				settings: doc
			});
		});
	}
	exports.saveSettings = function (req, res, next) {
		var data = req.data;
		Setting.findOne({}, function (err, doc) {
			if (err) return next(err);
			if (!doc) {
				saveSetting();
			} else {
				updateSetting(doc.id);
			}
		});

		function saveSetting() {
			var newSetting = new Setting();
			newSetting.name = data.name;
			newSetting.summary = data.summary;
			newSetting.notice = data.notice;
			newSetting.show_num = data.show_num;
			newSetting.save(function (err) {
				if (err) return next(err);
				res.json({
					success: 1,
					message: '保存成功!'
				});
			});
		}

		function updateSetting(id) {
			var condition = {
				'_id': id
			};
			var update = {
				$set: data
			}
			var option = {};
			Setting.update(condition, update, option, function (err) {
				if (err) return next(err);
				res.json({
					success: 1,
					message: '保存成功!'
				});
			});
		}
	}
	

} ());