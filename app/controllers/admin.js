( function () {
	
	'use strict';
	
	var models = require('../models');
	var Admin = models.Admin;
	var Post = models.Post;
	var _ = require('lodash');
	var async = require('async');
	var utility = require('utility');
	var validator = require('validator');
	var mail = require('../proxy/mail');

	exports.getAdmins = function (req, res, next) {
		var query = _.omit(req.data, 'pass', 'rePass', 'rePassword');
		Admin.find(query)
			.select('-retrieve_time -retrieve_key')
			.sort('-_id')
			.exec(function (err, users) {
				if (err) return next(err);
				res.json({
					success: 1,
					users: users
				});
			});
	}

	exports.getOneAdmin = function (req, res, next) {
		var id = _.isEmpty(req.params.id) ? req.User._id : req.params.id;
		var limit = Number(validator.trim(req.query.limit)) || 5; //Limit不是数字貌似也不会报错
		Admin.findOne({
				'_id': id
			})
			.select('-password')
			.exec(function (err, user) {
				if (err) return next(err);
				if (!user) {
					res.status(404);
					return res.json({
						success: 0,
						message: '没有这个用户！'
					});
				} else {
					Post.find({
							'author': id
						})
						.select('-md -comments')
						.sort('-_id')
						.limit(limit)
						.exec(function (err, posts) {
							if (err) return next(err);
							return res.json({
								success: 1,
								user: user,
								posts: posts
							});
						});
				}
			});
	}

	exports.updateUser = function (req, res, next) {
		var id = req.params.id || '';
		var data = _.omit(req.data, '_id', 'pass', 'rePass', 'rePassword');
		var update;

		function checkUser(cb) {
			Admin.findOne({
				'_id': id
			}, function (err, user) {
				if (err) return next(err);
				if (!user) {
					res.status(404);
					console.log(1);
					return cb('没有这个用户！');
				} else {
					return cb(null, user);
				}
			});
		}

		function checkPower(user, cb) {
			if (req.User.power < 1) {
				if (user._id.toString() !== req.User._id.toString()) {
					res.status(403);
					return cb('你只能修改你自己的资料');
				}
			} else {
				if (Number(user.power) === 2 && Number(req.User.power) !== 2) {
					res.status(403);
					return cb('你没有权限！');
				}
			}
			return cb(null, user);
		}

		function setUpdate(user, cb) {
			switch (req.User.power) {
			case 0:
				update = _.pick(data, 'name', 'bio', 'phone', 'qq', 'sex');
				break;
			case 1:
				update = _.pick(data, 'email', 'name', 'password', 'bio', 'phone', 'qq', 'sex', 'locked');
				break;
			case 2:
				update = _.omit(data, 'retrieve_time', 'retrieve_key');
				break;
			}
			cb(null, user, update);
		}

		function checkName(user, update, cb) {
			var User = user;
			if (update.name) {
				Admin.findOne({
					'name': update.name
				}, function (err, user) {
					if (err) return next(err);
					if (user && user._id.toString() !== id.toString()) {
						return cb('姓名已经被占用！');
					}
					return cb(null, User, update);
				});
			} else {
				return cb(null, User, update);
			}
		}

		function checkEmail(User, update, cb) {
			if (update.email) {
				Admin.findOne({
					'email': update.email
				}, function (err, user) {
					if (err) return next(err);
					if (user && user._id.toString() !== id.toString()) {
						return cb('此邮箱已被占用!');
					}
					return cb(null, User, update);
				});
			} else {
				return cb(null, User, update);
			}
		}

		function checkPass(User, update, cb) {
			if (update.password) {
				update.password = utility.sha1(update.password);
			}
			return cb(null, User, update);
		}

		function updateUser(User, update, cb) {
			if (_.isEmpty(update)) {
				return cb('没有收到任何改动请求！');
			} else {
				Admin.update({
					'_id': id
				}, {
					$set: update
				}, {}, function (err) {
					if (err) return next(err);
					if (Number(req.User.power) !== Number(User.power)) {
						//发送邮件
						var emailTo;
						if (update.email) {
							emailTo = update.email;
						} else {
							emailTo = User.email;
						}
						mail.sendUpdateUserMail(emailTo, req.User.name, req.User.email); //发送的是加密过后的密码!
					}
					cb(null);
				});
			}
		}

		async.waterfall([checkUser, checkPower, setUpdate, checkName, checkEmail, checkPass, updateUser], function (err) {
			if (err) {
				return res.json({
					success: 0,
					message: err
				});
			} else {
				return res.json({
					success: 1,
					message: '修改资料成功！'
				});
			}
		});
	}

	exports.removeUser = function (req, res, next) {
		var id = req.params.id;

		function checkUser(cb) {
			Admin.findOne({
				'_id': id
			}, function (err, user) {
				if (err) return next(err);
				if (!user) {
					res.status(404);
					return cb('没有这个用户！');
				} else {
					return cb(null, user);
				}
			});
		}

		function checkPost(user, cb) {
			if (user.active) {
				Post.find({
					'author': id
				}, function (err, posts) {
					if (err) return next(err);
					if (!_.isEmpty(posts)) {
						return cb('此用户有文章,无法删除!');
					} else {
						return cb(null);
					}
				});
			} else {
				return cb(null);
			}
		}

		function removeUser() {
			Admin.remove({
				'_id': id
			}, function (err) {
				if (err) return next(err);
				res.json({
					success: 1,
					message: '删除用户成功！'
				});
			});
		}

		async.waterfall([checkUser, checkPost], function (err) {
			if (err) {
				res.json({
					success: 0,
					message: err
				});
			} else {
				removeUser();
			}
		});
	}

}() );