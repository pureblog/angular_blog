(function () {

	'use strict';
	
	var Post = require('../models').Post;
	var Admin = require('../models').Admin;
	var async = require('async');
	var validator = require('validator');
	var slugify = require('transliteration').slugify;
	var _ = require('lodash');
	var utility = require('utility');

	exports.getClassify = function (req, res, next) {
		Post.find({}, function (err, docs) {
			if (err) return next(err);
			async.parallel({
				classify: function (cb) {
					var classify = [];
					var newClassify = [];
					docs.forEach(function (val) {
						if (val.classify.name) {
							classify.push(_.pick(val.classify, 'name', 'slug'));
						}
					});
					classify.forEach(function (val) {
						if (!newClassify.length) {
							val.total = 1;
							newClassify.push(val);
						} else {
							var equal = newClassify.some(function (d_val, index, arr) {
								if (_.isEqual(val, _.pick(d_val, 'name', 'slug'))) {
									arr[index].total++;
								}
								return _.isEqual(val, _.pick(d_val, 'name', 'slug'));
							});
							if (!equal) {
								val.total = 1;
								newClassify.push(val);
							}
						}
					});
					cb(null, newClassify);
				},
				tags: function (cb) {
					var tags = [];
					var flattenTags = [];
					var newTags = [];
					docs.forEach(function (val) {
						if (val.tags.length) {
							tags.push(val.tags);
						}
					});
					flattenTags = _.flatten(tags, true);
					flattenTags.forEach(function (val) {
						if (!newTags.length) {
							newTags.push(_.pick(val, 'name', 'slug'));
						} else {
							var equal = newTags.some(function (d_val) {
								return _.isEqual(_.pick(val, 'name', 'slug'), _.pick(d_val, 'name', 'slug'));
							});
							if (!equal) {
								newTags.push(_.pick(val, 'name', 'slug'));
							}
						}
					});
					cb(null, newTags);
				}
			}, function (err, result) {
				res.json({
					success: 1,
					classify: result.classify,
					tags: result.tags
				});
			});
		});
	}

	exports.getPosts = function (req, res, next) {
		var query = req.data || {};
		if (_.has(query, 'classify')) {
			query['classify.name'] = query.classify;
			query = _.omit(query, 'classify')
		}
		Post.find(query)
			.select('-md -html')
			.populate('author', 'name power slug sex avatar')
			.sort('-_id')
			.exec(function (err, posts) {
				if (err) return next(err);
				res.json({
					success: 1,
					posts: posts
				});
			});
	}

	exports.getOnePost = function (req, res, next) {
		var id = req.params.id || '';
		Post.findById(id)
			.select('-html')
			.populate('author', 'name power slug')
			.exec(function (err, post) {
				if (err) return next(err);
				if (!post) {
					res.status(404);
					return res.json({
						success: 0,
						message: '没有这条数据！'
					});
				} else {
					res.json({
						success: 1,
						post: post
					});
				}
			});
	}

	exports.newPost = function (req, res, next) {
		var data = req.data;
		var newPost = {};
		newPost.title = data.title ? validator.trim(data.title) : '未定义标题' + new Date().getTime();
		newPost.author = req.User._id;
		newPost.summary = data.summary;
		newPost.cover = data.cover || '';
		newPost.md = data.md;
		newPost.html = data.html;
		newPost.published = data.published || false;
		function checkSummary(cb) {
			if (data.published) {
				if (!data.summary) {
					return cb('发布前你必须为博文写简介！');
				}
			}
			cb(null);
		}

		function getSlug(cb) {
			Post.findOne({
				'title': newPost.title
			}, function (err, post) {
				if (err) return cb(err);
				if (post) {
					newPost.slug = slugify(newPost.title + new Date().getTime());
				} else {
					newPost.slug = slugify(newPost.title);
				}
				cb(null);
			});
		}

		function getClassify(cb) {
			if (data.classify) {
				newPost.classify = {
					name: data.classify,
					slug: slugify(data.classify)
				}
			}
			cb(null);
		}

		function getTags(cb) {
			if (!_.isEmpty(data.tags)) {
				var tags = [];
				for (var i = 0; i < data.tags.length; i++) {
					tags.push({
						name: validator.trim(data.tags[i]).toLowerCase(),
						slug: slugify(data.tags[i])
					});
				}
				newPost.tags = tags;
			}
			cb(null);
		}

		function getPub(cb) {
			if (newPost.published) {
				newPost.publish_at = new Date;
				return cb(null, '发布成功!');
			}
			cb(null, '保存成功！');
		}

		function savePost(msg) {
			var AddPost = new Post(newPost);
			AddPost.save(function (err) {
				if (err) return next(err);
				Post.findOne({
					'slug': newPost.slug
				}, function (err, doc) {
					if (err) return next(err);
					res.json({
						success: 1,
						message: msg,
						id: doc._id
					});
				});
			});
		}
		async.parallel([checkSummary, getSlug, getClassify, getTags, getPub], function (err, msg) {
			if (err) {
				return next(err);
			} else {
				var message = _.without(msg, undefined)[0];
				savePost(message);
			}
		});
	}

	exports.updatePost = function (req, res, next) {
		var id = req.params.id || '';
		var data = req.data;
		data.title = data.title ? validator.trim(data.title) : '未定义标题' + new Date().getTime();

		function getClassify(cb) {
			if (data.classify) {
				data.classify = {
					name: data.classify,
					slug: slugify(data.classify)
				}
			}
			cb(null);
		}

		function getTags(cb) {
			var tags = [];
			if (data.tags) {
				for (var i = 0; i < data.tags.length; i++) {
					tags.push({
						name: validator.trim(data.tags[i]).toLowerCase(),
						slug: slugify(data.tags[i])
					});
				}
				data.tags = tags;
			}
			cb(null);
		}

		function checkSummary(cb) {
			if (data.published) {
				if (!data.summary) {
					return cb('发布前你必须为博文写简介！');
				}
			}
			cb(null);
		}

		function checkPost(cb) {
			Post.findById(id, function (err, post) {
				if (err) return next(err);
				if (!post) {
					res.status(404);
					return cb('无法更新,没有这条数据！');
				} else if (Number(req.User.power) === 0 && req.User._id.toString() !== post.author.toString()) {
					res.status(403);
					return cb('你只能修改你自己的文章！');
				} else {
					if (data.published !== 'undefined' && data.published !== post.published) {
						if (data.published) {
							data.publish_at = new Date;
							return cb(null, '发布成功！');
						} else {
							return cb(null, '已撤销发布！');
						}
					} else {
						return cb(null, '已更新！');
					}
				}
			});
		}

		function updatePost(cb) {
			var update = _.omit(data, '_id', 'create_at');
			update.update_at = new Date;
			Post.update({
				'_id': id
			}, {
				$set: update
			}, {}, function (err) {
				if (err) return next(err);
				cb(null);
			});
		}

		async.series([checkSummary, getClassify, getTags, checkPost, updatePost], function (err, msg) {
			if (err) {
				return res.json({
					success: 0,
					message: err
				});
			} else {
				var message = _.without(msg, undefined)[0];
				return res.json({
					success: 1,
					message: message
				});
			}
		});
	}

	exports.removePost = function (req, res, next) {
		var id = req.query.id || '';

		function checkPost(cb) {
			Post.findById(id)
				.populate('author', 'power password')
				.exec(function (err, post) {
					if (err) return next(err);
					if (!post) {
						res.status(404);
						return cb('删除失败,没有这条数据！');
					}
					return cb(null, post);
				});
		}

		function checkPower(post, cb) {
			if (req.User._id.toString() !== post.author._id.toString()) { //不是自己的文章
				if (req.User.power < post.author.power) {
					res.status(403);
					return cb('你没有权限！');
				} else if (req.User.power === 0) {
					res.status(403);
					return cb('你只能删除你自己的文章!');
				}
			}
			cb(null, post);
		}

		function confirmPass(post, cb) {
			if (req.User._id.toString() !== post.author._id.toString()) {
				var password = req.query.pass || req.query.password || '';
				if (!password) {
					res.status(422);
					return cb('请输入密码！');
				} else {
					var pass = utility.sha1(password);
					Admin.findById(req.User._id, function (err, user) {
						if (err) return next(err);
						if (pass !== user.password) {
							res.status(422);
							return cb('密码不正确！');
						}
					});
				}
			}
			cb(null);
		}

		function removePost(cb) {
			Post.remove({
				'_id': id
			}, function (err) {
				if (err) return next(err);
				cb(null);
			});
		}

		async.waterfall([checkPost, checkPower, confirmPass, removePost], function (err) {
			if (err) {
				return res.json({
					success: 0,
					message: err
				});
			} else {
				res.json({
					success: 1,
					message: '删除成功！'
				});
			}
		});
	}
}());