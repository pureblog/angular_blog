(function () {

	'use strict';
	
	var Post = require('../models').Post;
	var Setting = require('../models').Setting;
	var async = require('async');
	var validator = require('validator');
	var _ = require('lodash');

	function doQuery(query, page, callback) {
		Setting.findOne({})
			.select('-notice')
			.exec(function (err, settings) {
				if (err) return callback(err);
				var limit = settings ? Number(settings.show_num) : 5;
				async.parallel({
					total: function (cb) {
						Post.find(query)
							.count(function (err, count) {
								if (err) return cb(err);
								if (!count) {
									return cb({
										status: 404,
										message: '没有找到数据！'
									});
								}
								cb(null, count);
							});
					},
					posts: function (cb) {
						Post.find(query)
							.select('-md -html')
							.populate('author', 'name slug avatar bio')
							.sort('-publish_at')
							.skip((page - 1) * limit)
							.limit(limit)
							.exec(function (err, posts) {
								if (err) return cb(err);
								if (!posts.length) {
									return cb({
										status: 404,
										message: '没有找到数据！'
									});
								}
								cb(null, posts);
							});
					}
				}, function (err, result) {
					if (err) {
						if (err.kind === 'ObjectId') {
							return callback({
								success: 0,
								status: 404,
								message: '没有找到数据！'
							});
						} else {
							return callback({
								success: 0,
								status: err.status || 500,
								message: err.message
							});
						}
					} else {
						return callback({
							success: 1,
							settings: settings,
							total: result.total,
							posts: result.posts
						});
					}
				});
			});
	}

	//settings
	exports.getSettings = function (req, res, next) {
		Setting.findOne({})
			.select('-notice')
			.exec(function (err, doc) {
				if (err) return next(err);
				res.json({
					success: 1,
					settings: doc
				});
			});
	}

	//posts
	exports.getClassify = function (req, res, next) {
		Post.find({
			'published': true
		}, function (err, docs) {
			if (err) return next(err);
			async.parallel({
				classify: function (cb) {
					var classify = [];
					var newClassify = [];
					docs.forEach(function (val) {
						if (val.classify.name) {
							classify.push(_.pick(val.classify, 'name', 'slug'));
						}
					});
					classify.forEach(function (val) {
						if (!newClassify.length) {
							val.total = 1;
							newClassify.push(val);
						} else {
							var equal = newClassify.some(function (d_val, index, arr) {
								if (_.isEqual(val, _.pick(d_val, 'name', 'slug'))) {
									arr[index].total++;
								}
								return _.isEqual(val, _.pick(d_val, 'name', 'slug'));
							});
							if (!equal) {
								val.total = 1;
								newClassify.push(val);
							}
						}
					});
					cb(null, newClassify);
				},
				tags: function (cb) {
					var tags = [];
					var flattenTags = [];
					var newTags = [];
					docs.forEach(function (val) {
						if (val.tags.length) {
							tags.push(val.tags);
						}
					});
					flattenTags = _.flatten(tags, true);
					flattenTags.forEach(function (val) {
						if (!newTags.length) {
							newTags.push(_.pick(val, 'name', 'slug'));
						} else {
							var equal = newTags.some(function (d_val) {
								return _.isEqual(_.pick(val, 'name', 'slug'), _.pick(d_val, 'name', 'slug'));
							});
							if (!equal) {
								newTags.push(_.pick(val, 'name', 'slug'));
							}
						}
					});
					cb(null, newTags);
				}
			}, function (err, result) {
				res.json({
					success: 1,
					classify: result.classify,
					tags: result.tags
				});
			});
		});
	}

	exports.getOnePostBySlug = function (req, res, next) {
		var slug = validator.trim(req.params.slug);
		var query = {
			'published': true,
			'slug': slug
		};
		Post.findOne(query)
			.select('-md')
			.populate('author', 'name slug')
			.exec(function (err, post) {
				if (err) return next(err);
				if (!post) {
					res.status(404);
					return res.json({
						success: 0,
						message: '没有找到数据！'
					});
				}
				res.json({
					success: 1,
					post: post
				});
			});
	}

	exports.getPostsLimit = function (req, res) {
		var page = req.params.page || 1;
		var query = {
			'published': true
		};
		if (page < 1) {
			return res.status(404).json({
				success: 0,
				message: '页面不存在！'
			});
		}
		doQuery(query, page, function (data) {
			if (!data.success) {
				res.status(data.status);
			}
			res.json(data);
		});
	}

	exports.getRecentPost = function (req, res, next) {
		var query = {
			'published': true
		};
		var limit = req.params.limit || 5;
		Post.find(query)
			.select('title slug update_at')
			.populate('author', 'name slug')
			.sort('-update_at')
			.limit(limit)
			.exec(function (err, posts) {
				if (err) return next(err);
				if (!posts.length) {
					return res.status(404).json({
						success: 0,
						message: '没有找到数据！'
					});
				}
				return res.json({
					success: 1,
					posts: posts
				});
			});
	}

	exports.getPostsLimitByClassify = function (req, res) {
		var page = req.params.page || 1;
		var classify = req.params.classify;
		var query = {
			'published': true,
			'classify.slug': classify
		};
		if (page < 1) {
			return res.status(404).json({
				success: 0,
				message: '页面不存在！'
			});
		}
		doQuery(query, page, function (data) {
			if (!data.success) {
				res.status(data.status);
			}
			res.json(data);
		});
	}
	exports.getPostsLimitByTag = function (req, res) {
		var page = req.params.page || 1;
		var tag = req.params.tag;
		var query = {
			'published': true,
			'tags': {
				'$elemMatch': {
					'slug': tag
				}
			}
		};
		if (page < 1) {
			return res.status(404).json({
				success: 0,
				message: '页面不存在！'
			});
		}
		doQuery(query, page, function (data) {
			if (!data.success) {
				res.status(data.status);
			}
			res.json(data);
		});
	}
	exports.getPostsLimitByAuthor = function (req, res) {
		var page = req.params.page || 1;
		var author = req.params.author;
		var query = {
			'published': true,
			'author': author
		};
		if (page < 1) {
			return res.status(404).json({
				success: 0,
				message: '页面不存在！'
			});
		}
		doQuery(query, page, function (data) {
			if (!data.success) {
				res.status(data.status);
			}
			res.json(data);
		});
	}


}());