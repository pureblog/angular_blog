(function (){

	'use strict';
	
	var config = require('../config');
	var models = require('../models');
	var mailer = require('../proxy/mail');
	var Admin = models.Admin;
	var async = require('async');
	var utils = require('utility');
	var jwt = require('jsonwebtoken');
	var slugify = require('transliteration').slugify;
	var uuid = require('uuid');
	var utility = require('utility');
	var _ = require('lodash');

	exports.signin = function (req, res) {
		var data = req.data;

		function checkEmpty(cb) {
			if (_.isEmpty(data.email) || _.isEmpty(data.pass)) {
				return cb('账号或密码必填！');
			}
			cb(null);
		}

		function findUser(cb) {
			Admin.findOne({
				'email': data.email
			}, function (err, user) {
				if (!user || !user.active) {
					return cb('用户不存在!');
				}
				cb(err, user);
			});
		}

		function checkPass(user, cb) {
			if (utils.sha1(data.pass) !== user.password) {
				return cb('密码不正确!');
			}
			cb(null, user);
		}

		function checkLock(user, cb) {
			if (user.locked) {
				return cb('此账户已被锁定！');
			}
			cb(null, user);
		}

		function updateUser(user, cb) {
			user.update_at = new Date();
			user.save(function (err) {
				return cb(err, user);
			});
		}

		function createToken(user, cb) {
			var token = jwt.sign({
					_id: user._id,
					name: user.name,
					email: user.email,
					password: user.password,
					power: user.power
				},
				config.token_key, {
					'expiresIn': config.token_lasts + 'd'
				});
			cb(null, token, user);
		}
		async.waterfall([checkEmpty, findUser, checkPass, checkLock, updateUser, createToken], function (err, token, user) {
			if (err) {
				return res.status(422).json({
					success: 0,
					message: err
				});
			} else {
				res.json({
					success: 1,
					token: token,
					user: {
						_id: user._id,
						name: user.name,
						power: user.power
					}
				});
			}
		});
	}

	exports.showSignup = function (req, res) {
		return res.json({
			success: 1
		});
	}

	exports.signup = function (req, res, next) {
		var data = req.data;
		var pass = data.pass || data.password;
		var rePass = data.rePass || data.rePassword;

		function checkEmpty(cb) {
			if (_.isEmpty(data.name) || _.isEmpty(data.email) || _.isEmpty(pass) || _.isEmpty(rePass) || _.isEmpty(data.sex.toString())) {
				return cb('信息不完整！');
			}
			cb(null);
		}

		function checkEmail(cb) {
			if (data.email !== req.User.email) {
				return cb('你填写的邮箱和被邀请的邮箱不一致！');
			}
			cb(null);
		}

		function checkName(cb) {
			Admin.findOne({
				'name': data.name
			}, function (err, user) {
				if (user) {
					return cb('姓名已经被占用');
				}
				cb(null);
			});
		}

		function updateUser() {
			var condition = {
				'_id': req.User._id
			};
			var update = {
				$set: {
					name: data.name,
					slug: slugify(data.name),
					sex: data.sex,
					password: utils.sha1(data.pass),
					active: true,
					retrieve_key: null,
					retrieve_time: null
				}
			}
			var option = {};
			Admin.update(condition, update, option, function (err) {
				if (err) return next(err);
				res.json({
					success: 1,
					message: '注册成功！'
				});
			});
		}

		function createUser(cb) {
			var Master = new Admin();
			Master.name = data.name;
			Master.email = data.email;
			Master.slug = slugify(data.name);
			Master.password = utils.sha1(data.pass);
			Master.active = true;
			Master.sex = data.sex;
			Master.power = 2;
			Master.save(function (err) {
				if (err) return next(err);
				cb(null);
			});
		}
		if (req.User.power !== 2) {
			async.parallel([checkEmpty, checkEmail, checkName], function (err) {
				if (err) {
					return res.status(422).json({
						success: 0,
						message: err
					});
				} else {
					updateUser();
				}
			});
		} else {
			async.series([checkEmpty, createUser], function (err) {
				if (err) {
					return res.status(422).json({
						success: 0,
						message: err
					});
				} else {
					res.json({
						success: 1,
						message: '注册成功!'
					});
				}
			});
		}
	}

	exports.inviteUser = function (req, res, next) {
		var email = req.data.email;
		var power = req.data.power || 0;

		function checkPower(cb) {
			if (power >= req.User.power) {
				return cb('你的权限不够！');
			}
			return cb(null);
		}

		function checkEmail(cb) {
			if (!email) {
				return cb('邮箱必填！');
			} else {
				Admin.findOne({
					'email': email
				}, function (err, user) {
					if (err) return next(err);
					if (user) {
						return cb('此邮箱已被注册！');
					} else {
						cb(null);
					}
				});
			}
		}

		function addUser() {
			var retrieveKey = uuid.v4();
			var retrieveTime = new Date().getTime();
			var newUser = new Admin();
			newUser.email = email;
			newUser.power = power;
			newUser.active = false,
			newUser.retrieve_key = retrieveKey;
			newUser.retrieve_time = retrieveTime;
			newUser.save(function (err) {
				if (err) return next(err);
				mailer.sendInviteMail(email, req.User.name, retrieveKey);
				return res.json({
					success: 1,
					message: '给' + email + '发送邀请注册邮件成功!'
				});
			});
		}

		async.parallel([checkPower, checkEmail], function (err) {
			if (err) {
				return res.json({
					success: 0,
					message: err
				});
			} else {
				addUser();
			}
		});
	}

	exports.applyReset = function (req, res, next) {
		var email = req.data.email;

		function checkEmail(cb) {
			if (!email) {
				return cb('请填写需要找回密码的邮箱！');
			} else {
				Admin.findOne({
					'email': email,
					'active': true
				}, function (err, user) {
					if (err) return next(err);
					if (!user) {
						return cb('没有这个用户!');
					} else {
						return cb(null, user);
					}
				});
			}
		}

		function sendEmail(user, cb) {
			var retrieveKey = uuid.v4();
			var retrieveTime = new Date().getTime();
			user.retrieve_key = retrieveKey;
			user.retrieve_time = retrieveTime;
			user.save(function (err) {
				if (err) return next(err);
				mailer.sendResetPassMail(email, user.name, retrieveKey);
				return cb(null, '我们给' + email + '发送了一封邮件,请在24小时内点击里面的链接重置密码！');
			});
		}
		async.waterfall([checkEmail, sendEmail], function (err, msg) {
			if (err) {
				res.json({
					success: 0,
					message: err
				});
			} else {
				res.json({
					success: 1,
					message: msg
				});
			}
		});
	}

	exports.showReset = function (req, res) {
		res.json({
			success: 1
		});
	}

	exports.resetPass = function (req, res, next) {
		var pass = req.data.pass;
		var rePass = req.data.rePass;

		function checkPass(cb) {
			if (!pass || !rePass) {
				return cb('请填写新密码！');
			} else {
				return cb(null);
			}
		}

		function resetPass(cb) {
			var condition = {
				'_id': req.User._id
			};
			var update = {
				$set: {
					password: utility.sha1(pass),
					retrieve_key: null,
					retrieve_time: null
				}
			}
			var option = {};
			Admin.update(condition, update, option, function (err) {
				if (err) return next(err);
				cb(null);
			});
		}

		async.series([checkPass, resetPass], function (err) {
			if (err) {
				return res.status(422).json({
					success: 0,
					message: err
				});
			} else {
				res.json({
					success: 1,
					message: '你的密码已经成功重置!'
				});
			}
		});
	}

} ());