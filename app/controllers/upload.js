(function () {

	'use strict';
	
	var fs = require('fs');
	var path = require('path');
	var multiparty = require('multiparty');
	var uuid = require('uuid');
	var sharp = require('sharp');
	var _ = require('lodash');
	var async = require('async');
	var makeDirSync = require('../proxy/utils').makeDirSync;
	var Admin = require('../models').Admin;
	var Setting = require('../models').Setting;
	var exec = require('child_process').exec;

	function checkImage(file, limitSize, format, callback) {
		var type = file.headers['content-type'].split('/')[1];
		if (file.size > limitSize) {
			return callback('上传失败！图片最大支持' + limitSize / 1024 / 1024 + 'M!');
		}
		if (_.indexOf(format, type) === -1) {
			return callback('不支持的图片格式!');
		}
		return callback(null);
	}

	function checkPower(reqUser, id, callback) {
		Admin.findById(id, function (err, user) {
			if (err) return callback(err);
			if (!user) {
				return callback({
					status: 404,
					success: 0,
					message: 'user is not exit!'
				});
			} else {
				if (reqUser.power < 1) {
					if (id.toString() !== reqUser._id.toString()) {
						return callback({
							status: 403,
							success: 0,
							message: '你只能修改你自己的资料'
						});
					} else {
						return callback(null);
					}
				} else {
					if (Number(user.power) === 2 && Number(reqUser.power) !== 2) {
						return callback({
							status: 403,
							success: 0,
							message: '你没有权限！'
						});
					} else {
						return callback(null);
					}
				}
			}
		});
	}

	exports.reportBug = function (req, res, next) {
		var tmpDir = path.join(__dirname, '../upload/tmp/');
		var bugDir = path.join(__dirname, '../upload/bug/');
		var format = ['jpeg', 'png', 'webp'];
		var option = {
			maxFieldsSize: 5 * 1024 * 1024,
			autoFields: true,
			uploadDir: tmpDir
		}
		var form = new multiparty.Form(option);
		makeDirSync(tmpDir);
		makeDirSync(bugDir);
		form.on('error', function (err) {
			console.log(err);
		});
		form.on('file', function (name, file) {
			checkImage(file, option.maxFieldsSize, format, function (err) {
				if (err) {
					return res.json({
						success: 0,
						message: err
					});
				} else {
					var fileName = uuid.v4();
					var target_path = path.join(__dirname, '../upload/bug/' + fileName);
					sharp(file.path)
						.rotate()
						.resize(1000, 600)
						.max()
						.quality(90)
						.toFile(target_path, function (err) {
							if (err) return next(err);
							res.json({
								success: 1,
								message: '上传成功！',
								url: '/upload/bug/' + fileName
							});
						});
				}
			});
		});
		form.parse(req);
	}

	exports.post = function (req, res, next) {
		var tmpDir = path.join(__dirname, '../upload/tmp/');
		var postDir = path.join(__dirname, '../upload/post/');
		var format = ['jpeg', 'png', 'webp'];
		var option = {
			maxFieldsSize: 5 * 1024 * 1024,
			autoFields: true,
			uploadDir: tmpDir
		}
		var form = new multiparty.Form(option);
		makeDirSync(tmpDir);
		makeDirSync(postDir);
		form.on('error', function (err) {
			console.log(err);
		});
		form.on('file', function (name, file) {
			checkImage(file, option.maxFieldsSize, format, function (err) {
				if (err) {
					return res.json({
						success: 0,
						message: err
					});
				} else {
					var fileName = uuid.v4();
					var target_path = path.join(__dirname, '../upload/post/' + fileName);
					sharp(file.path)
						.rotate()
						.resize(800, 500)
						.max()
						.quality(90)
						.toFile(target_path, function (err) {
							if (err) return next(err);
							exec('rm -rf ' + tmpDir, function (err) {
								if (err) return next(err);
								res.json({
									success: 1,
									message: '上传成功！',
									url: '/upload/post/' + fileName
								});
							});
						});
				}
			});
		});
		form.parse(req);
	}
	exports.postCover = function (req, res, next) {
		var tmpDir = path.join(__dirname, '../upload/tmp/');
		var postCoverDir = path.join(__dirname, '../upload/postCover/');
		var format = ['jpeg', 'png', 'webp'];
		var option = {
			maxFieldsSize: 5 * 1024 * 1024,
			autoFields: true,
			uploadDir: tmpDir
		}
		var form = new multiparty.Form(option);
		makeDirSync(tmpDir);
		makeDirSync(postCoverDir);
		form.on('error', function (err) {
			console.log(err);
		});
		form.on('file', function (name, file) {
			checkImage(file, option.maxFieldsSize, format, function (err) {
				if (err) {
					return res.json({
						success: 0,
						message: err
					});
				} else {
					var fileName = uuid.v4();
					var target_path = path.join(__dirname, '../upload/postCover/' + fileName);
					sharp(file.path)
						.rotate()
						.resize(300, 300)
						.max()
						.quality(90)
						.toFile(target_path, function (err) {
							if (err) return next(err);
							exec('rm -rf ' + tmpDir, function (err) {
								if (err) return next(err);
								res.json({
									success: 1,
									message: '上传成功！',
									url: '/upload/postCover/' + fileName
								});
							});
						});
				}
			});
		});
		form.parse(req);
	}
	exports.avatar = function (req, res, next) {
		var id = req.params.id;
		var tmpDir = path.join(__dirname, '../upload/tmp/');
		var avatarDir = path.join(__dirname, '../upload/avatar/');
		var format = ['svg+xml', 'jpeg', 'png', 'webp'];
		var option = {
			maxFieldsSize: 5 * 1024 * 1024,
			autoFields: true,
			uploadDir: tmpDir
		}
		var form = new multiparty.Form(option);
		makeDirSync(tmpDir);
		makeDirSync(avatarDir);
		form.on('error', function (err) {
			console.log(err);
		});
		form.on('file', function (name, file) {
			checkImage(file, option.maxFieldsSize, format, function (err) {
				if (err) {
					return res.json({
						success: 0,
						message: err
					});
				} else {
					var fileName = id + '.svg';
					var fileName_L = id + '.large';
					var fileName_S = id + '.small';
					var target_path = path.join(__dirname, '../upload/avatar/' + fileName);
					var target_path_L = path.join(__dirname, '../upload/avatar/' + fileName_L);
					var target_path_S = path.join(__dirname, '../upload/avatar/' + fileName_S);
					checkPower(req.User, id, function (err) {
						if (err) {
							res.status(err.status);
							return res.json(err);
						} else {
							var type = file.headers['content-type'].split('/')[1];
							if (type === 'svg+xml') {
								if (file.size > 0.06 * 1024 * 1024) {
									return res.json({
										success: 0,
										message: 'max svg image 60Kb !'
									});
								} else {
									fs.renameSync(file.path, target_path, function (err) {
										if (err) {
											console.error(err.stack);
											return res.json({
												success: 0,
												message: '上传失败！'
											});
										}
									});
									var img_url = '/upload/avatar/' + fileName;
									Admin.update({
										'_id': id
									}, {
										$set: {
											'avatar': img_url
										}
									}, {}, function (err) {
										if (err) return next(err);
										return res.json({
											success: 1,
											message: '上传成功！',
											url: '/upload/avatar/' + fileName,
										});
									});
								}
							} else {
								async.parallel([
									function (cb) {
										sharp(file.path)
											.rotate()
											.resize(400, 400)
											.max()
											.quality(90)
											.toFile(target_path_L, function (err) {
												if (err) return cb(err);
												return cb(null);
											});
									},
									function (cb) {
										sharp(file.path)
											.rotate()
											.resize(200, 200)
											.max()
											.quality(100)
											.toFile(target_path_S, function (err) {
												if (err) return cb(err);
												return cb(null);
											});
									}
								], function (err) {
									if (err) return next(err);
									var img_url = '/upload/avatar/' + fileName_S;
									Admin.update({
										'_id': id
									}, {
										$set: {
											'avatar': img_url
										}
									}, {}, function (err) {
										if (err) return next(err);
										exec('rm -rf ' + tmpDir, function (err) {
											if (err) return next(err);
											return res.json({
												success: 1,
												message: '上传成功！',
												url: img_url,
											});
										});
									});
								});
							}
						}
					});
				}
			});
		});
		form.parse(req);
	}
	exports.cover = function (req, res, next) {
		var tmpDir = path.join(__dirname, '../upload/tmp/');
		var coverDir = path.join(__dirname, '../upload/cover/');
		var format = ['jpeg', 'png', 'webp'];
		var option = {
			maxFieldsSize: 5 * 1024 * 1024,
			autoFields: true,
			uploadDir: tmpDir
		}
		var form = new multiparty.Form(option);
		makeDirSync(tmpDir);
		makeDirSync(coverDir);
		form.on('error', function (err) {
			console.error(err);
		});
		form.on('file', function (name, file) {
			checkImage(file, option.maxFieldsSize, format, function (err) {
				if (err) {
					return res.json({
						success: 0,
						message: err
					});
				} else {
					var fileName = 'cover';
					var target_path = path.join(__dirname, '../upload/cover/' + fileName);
					sharp(file.path)
						.rotate()
						.resize(1500)
						.max()
						.quality(80)
						.toFile(target_path, function (err) {
							if (err) return next(err);
							exec('rm -rf ' + tmpDir, function (err) {
								if (err) return next(err);
								Setting.findOne({}, function (err, doc) {
									if (err) return next(err);
									if (doc) {
										Setting.update({
											'_id': doc._id
										}, {
											'img_cover': '/upload/cover/' + fileName
										}, {}, function (err) {
											if (err) return next(err);
										});
									} else {
										var newSetting = new Setting();
										newSetting.img_cover = '/upload/cover/' + fileName;
										newSetting.save(function (err) {
											if (err) return next(err);
										});
									}
									res.json({
										success: 1,
										message: 'upload success!',
										url: '/upload/cover/' + fileName
									});
								});
							});
						});
				}
			});
		});
		form.parse(req);
	}
	

}());